import React, { Component } from 'react'
import { br, nbsp, stitch, findSort, insertOne, updateOne } from '../index';
import { ObjectId } from 'bson';
import { BlogUtils } from './BlogUtils';

const dbname = 'domingo';
const collection = 'blog';
const iconNuevo = (<i className="material-icons" title="añadir nueva sesión"> add_circle_outline </i> );
// const iconModificar = (<i className="material-icons" title="modificar sesión"> cloud_done </i> );
const iconModificar = (<i className="material-icons" title="modificar sesión"> done_outline </i> );
const iconPreview = (<i className="material-icons" title="previsualizar"> personal_video </i> );
// const iconPreview = (<i className="material-icons" title="previsualizar"> pageview </i> );


class Sesion {
  constructor(){
    this.titulo = '';
    this.autor = stitch.auth.user.id;
    this.seccion = '';
    this.html = '';
    this.activo = true;
    this.ediciones = 1
  }
}

export class Clase extends Component {
  // this.props.match.params.id
  constructor(props){
    super(props);
    this.state = {
      doc: new Sesion(),
      hideDebug: true,
      modifiedCount: 0,
      insertedId: '',
      hideEdit: true
    }
    // cerrar la modal con click fuera: 
    window.onclick = (event) => {  
      if (event.target ===  document.querySelector('#modalPreview') ) { 
        document.querySelector('#modalPreview').style.display = 'none'
      }
    }        
  }
  componentDidMount(){
    if ( this.props.match.params.id === 'nuevo'){} 
    else { this.getOne() }
  }
  
  // funciones DB
  getOne = n => {
    let params = { 
      db: dbname, 
      collection: collection,
      find: { _id: new ObjectId(this.props.match.params.id) }, 
      sort:{}
    }
    findSort(params)
    .then( arrayDatos => this.setState({ doc: arrayDatos[0] }));
  }

  getElNew = n => {
    let params = { 
      db: dbname, 
      collection: collection,
      find: { _id: new ObjectId(this.state.insertedId) }, 
      sort:{}
    }
    findSort(params)
    .then( arrayDatos => this.setState({ doc: arrayDatos[0] }));
  }


  insertOne = n => {
    // el insertOne quita la _id haya o no;
    let docSinId = this.state.doc;
    delete docSinId._id;
    this.setState({ doc: docSinId })
    let params = { db: dbname, collection: collection, doc: this.state.doc }
    insertOne(params)
    .then( r => { 
      console.log(r); 
      this.setState({ insertedId: r.insertedId.toString()});
      this.getElNew();
    });
  }  
  
  updateOne = n => {
    let params = { db: dbname, collection: collection, doc: this.state.doc }
    updateOne(params)
    .then( r => { console.log(r); this.setState({ 
      modifiedCount: this.state.modifiedCount + 1      
    }) });
  }
  
  // EVentos
  eventCambioInput = e => {
    this.setState({ doc: {...this.state.doc, [e.target.name]: e.target.value} });
  }


// Formularios y vistas  
  formCambios = e => {
    return (
      <div 
        hidden={this.state.modifiedCount === 0 }       
        title="respuesta del server: modifiedCount"  
        style={ {float: 'right', fontSize: 15 } }>        
        <span className="circle"> { this.state.modifiedCount } </span>
      </div>)
  }

  formInsertados = n => {
    return (
      <div 
        hidden={this.state.insertedId === '' }       
        title="respuesta del server: insertedId"  
        style={ {float: 'right', fontSize: 15 } }>           
        <span className="circle"> { this.state.insertedId } </span>                
      </div>
    )
  }

  
  formClase = n => {
    return (<div className="container">
      <br />
      <h4 
        onClick={ e => this.setState({ hideEdit: !this.state.hideEdit })}
        title={`${this.state.doc.ediciones} ediciones`}
        className="cpointer"> 
        {this.state.doc.titulo} 
      </h4>
      
      { this.formCambios() }
      { this.formInsertados() }

      <div hidden={this.state.hideEdit} className="form-inline">
        <input 
          name="titulo"
          value={this.state.doc.titulo}
          onChange={ this.eventCambioInput }
          placeholder="nombre de sesión"
          className="form-control"
          size="80"
        />
        {nbsp(3)}
        
        <span 
          hidden={ this.state.insertedId.length !== 0 }          
          onClick={ this.insertOne }> 
          { iconNuevo } 
        </span>

        {nbsp(2)}
        <span           
          onClick={ e => { this.setState({
            doc: {...this.state.doc, ediciones: this.state.doc.ediciones ? this.state.doc.ediciones + 1 : 1 }
          }, this.updateOne )  } }> 
          { iconModificar } 
        </span>
      </div>

    <div hidden={this.state.hideEdit} className="form-inline">
      Activo: {nbsp(1)}
      <input
          type="checkbox"
          checked={this.state.doc.activo}
          onChange={e => this.setState({ doc: { ...this.state.doc, activo: !this.state.doc.activo } })}
        />
        {nbsp(2)}
      Debug: {nbsp(1)}
      <input
          type="checkbox"
          onChange={e => this.setState({ hideDebug: !this.state.hideDebug })}
        />
        {nbsp(2)}
      <input
          name="seccion"
          onChange={this.eventCambioInput}
          value={this.state.doc.seccion}
          placeholder="sección"
          className="form-control"
        />
    </div>

    <BlogUtils />
    {/* botón para el preview del documento */}
    {br(1)}
    
    <span onClick={ e => document.querySelector('#modalPreview').style = 'display: block' }>
      {iconPreview}
    </span>

    {br(2)}

    {/* textarea para modificar el artículo */}
    <textarea
      name="html"
      onChange={ this.eventCambioInput }
      value={this.state.doc.html}
      placeholder="escribe html"
      className="form-control"  
      rows="20"
    />

    </div>)
  }

  modalPreview = e => {
    return (
    <div className="modal2" id="modalPreview">
      <div className="modal2-content">
        <h4> {this.state.doc.titulo} </h4>
        <small> {this.state.doc.seccion} </small>
        <hr />
        <div dangerouslySetInnerHTML={ {__html: this.state.doc.html} }/>
      </div>
    </div>
    )
  }

  render() {
    return (
      <div>
        {this.formClase()}
        {this.modalPreview()}
        <pre hidden={this.state.hideDebug} className="container"> {JSON.stringify(this.state, undefined, 2)} </pre>
      </div>
    )
  }
}
