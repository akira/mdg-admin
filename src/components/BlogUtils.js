import React, { Component } from 'react'
import {br, nbsp, uploadFile } from '../index'
// iconos de Angular Material
const iconCamara = <i className="material-icons" title="copiar HTML"> colorize </i>
const iconNuevo = (<i className="material-icons" title="añadir"> add_circle_outline </i> );
const iconCerrar = (<i className="material-icons" title="ocultar tool"> unfold_less </i> );

export class BlogUtils extends Component {
  state = { 
    tool: <span />, // la herramienta que se está mostrando
    hideTool: true // toggle herramienta actual
  }

  getTool = (id) => {
    // devuelve el componente a renderizar según el atributo "id" del <span> que lo muestra
    switch (id) {
      case 'BlogLista':
        return <BlogLista />    
      case 'BlogImg':
        return <BlogImg />    
      case 'BlogLink':
        return <BlogLink />        
      case 'BlogPre':
        return <BlogPre />          
      case 'BlogAudio':
        return <BlogAudio />
      case 'BlogVideo':
        return <BlogVideo />        
      case 'BlogParrafo':
        return <BlogParrafo />        
    
      default:
        return <span />
    }    
  }

  showTool = e => {
    // cambia la herramienta actual y la muestra;
    // muy interesante: en state también se puede guardar JSX, aquí en tool
    this.setState({ 
      tool: this.getTool(e.target.id), // según el span que se clicke muestra una herramienta
      hideTool: false // y enciende el div sí o sí
    })
  }

  render() {
    let css = {padding: 20}
    return (
      <div style={css}>
      {/* mantengo aquí <UploadFile /> por que lo pueden usar todas las tools */}
      <UploadFile />
      {/* herramientas */}
      <span key={1} id="BlogParrafo" onClick={ this.showTool } className="cpointer"> párrafo </span> {nbsp(5)}
      <span key={2} id="BlogLista" onClick={ this.showTool } className="cpointer"> lista </span> {nbsp(5)}
      <span key={3} id="BlogImg" onClick={ this.showTool } className="cpointer"> imagen </span> {nbsp(5)}
      <span key={4} id="BlogLink" onClick={ this.showTool } className="cpointer"> enlace </span> {nbsp(5)}
      <span key={5} id="BlogPre" onClick={ this.showTool } className="cpointer"> código </span> {nbsp(5)}
      <span key={6} id="BlogAudio" onClick={ this.showTool } className="cpointer"> audio </span> {nbsp(5)}
      <span key={7} id="BlogVideo" onClick={ this.showTool } className="cpointer"> vídeo </span> {nbsp(5)}

      {/* toggle herramienta */}
      <span onClick={ e => this.setState({ hideTool: !this.state.hideTool }) } className="cpointer"> {iconCerrar} </span> {nbsp(5)}
      <hr />      

      {/* mostrar la herramienta seleccionada */}
      <div hidden={this.state.hideTool}> 
        {this.state.tool}
      </div>

      </div>
    )
  }
}


export class UploadFile extends Component {
  state = { url: '' }
  render() {
    let css = {  }
    let cssInfo = { fontSize: 10, color: 'green'}
    return (
      <div style={css}>
        {/* <div className="form-inline"> */}
        <span onClick={ e => { document.getElementById('imagen').click() }} className="cpointer"> Storage </span>
        {nbsp(5)}
        {/* un enlace para abrir el archivo en pestaña nueva y decir ok upload */}
        <small hidden={this.state.url === '' } className="text-success"> 
          upload OK, url en clipboard {nbsp(5)}
          <a className="cpointer" href={ this.state.url } target="_blank" rel="noopener noreferrer"> 
            ver archivo 
          </a>        
        </small>        
        {/* el input type="file" real */}
        <input type="file" id="imagen" hidden={true}
          onClick={ e => this.setState({url: '' })}
          onChange={ e => { 
            if (e.target.files[0]) {
              uploadFile(e.target.files[0]).then( url => {
                this.setState({ url: url })
                navigator.clipboard.writeText(url).then( n => {})
              })
            }
          }}
          style={{ fontSize: 10}}
          className="form-control"/> 
        {/* </div> */}
        
        {/* mostrar la url cuando se tenga, también click para copiar */}
        <div 
          onClick={e => navigator.clipboard.writeText(this.state.url).then( n => {}) } 
          hidden={this.state.url === ''}
          title="click para copiar"
          className="cpointer"
          style={cssInfo}>     

          {this.state.url}    
        </div>


      </div>
    )
  }
}


export class BlogParrafo extends Component {
  state = {
    titulo: '', tamanyo: 3, texto: '',
    copiado: ''
  }

  render() {
    let css = { padding: 5, backgroundColor: '' }
    return (
      <div style={css}>
        escribir párrafo {nbsp(10)}
        {/* botones copiar */}
        <span
          onClick={e => {
            let txt = `<h${this.state.tamanyo} class=""> ${this.state.titulo} </h${this.state.tamanyo}>\n<p>\n${this.state.texto}\n</p>`
            navigator.clipboard.writeText(txt).then(e => this.setState({ copiado: 'done_outline' }));
            setTimeout(e => this.setState({ copiado: '' }), 500)
          }}> {iconCamara} </span>

        <i className="material-icons text-success" hidden={this.state.copiado === ''}>
          {this.state.copiado}
        </i>

        
        {/* input titulo y tamaño */}
        <div className="form-inline">
          <input onChange={ e => this.setState({ titulo: e.target.value })} className="form-control" size="50" />
          {nbsp(5)}
          tamaño título {nbsp(2)}
          &lt;h <input 
            onChange={ e => this.setState({ tamanyo: e.target.value }) } 
            value = { this.state.tamanyo }
            type="number" min="1" max="8" /> /&gt;
        </div>
        

        {/* párrafo */}
        <textarea 
          onChange={e => {
            this.setState({ texto: e.target.value })
          }} 
          rows="8"
          className="form-control"/>

      </div>
    )
  }
}


export class BlogAudio extends Component {
  state = { src: '', copiado: '', height: 50, width: 50 }

  render() {
    let css = { padding: 20, backgroundColor: '', width: '100%' }

    return (
      <div style={css}>        
        {/* titulo del componente */}
        crear HTML audio {nbsp(10)}
        {/* botones de copiar */}  
        <span
          onClick={e => {
            let txt = `<audio controls height="${this.state.height}%" width="${this.state.width}%">\n<source src="${this.state.src}">\n</audio>`
            navigator.clipboard.writeText(txt).then(e => this.setState({ copiado: 'done_outline' }));
            setTimeout(e => this.setState({ copiado: '' }), 500)
          }}> {iconCamara} </span>
        
        <i className="material-icons text-success" hidden={ this.state.copiado === ''}>
          { this.state.copiado }
        </i>

        {/* inputs para dimensiones */}
        <div className="form-inline">
          alto: {nbsp(3)} 
          <input 
            onChange={ e => this.setState({ height: e.target.value })}
            value={this.state.height}
            type="number"
            className="form-control" 
            min="1" max="100" />%
          {nbsp(10)}
          ancho: {nbsp(3)}
          <input 
            onChange={ e => this.setState({ width: e.target.value })}
            value={this.state.width}
            type="number" 
            className="form-control" 
            min="1" max="100" />%
        </div>

        {/* input para el src de la imagen */}
        <input 
          onChange={e => this.setState({ src: e.target.value })} 
          className="form-control"
          placeholder='src'/>
      </div>
    )
  }
}

export class BlogVideo extends Component {
  state = { src: '', copiado: '', height: 50, width: 50 }

  render() {
    let css = { padding: 20, backgroundColor: '', width: '100%' }

    return (
      <div style={css}>        
        {/* titulo del componente */}
        crear HTML vídeo {nbsp(10)}
        {/* botones de copiar */}  
        <span
          onClick={e => {
            let txt = `<video controls height="${this.state.height}%" width="${this.state.width}%">\n<source src="${this.state.src}">\n</audio>`
            navigator.clipboard.writeText(txt).then(e => this.setState({ copiado: 'done_outline' }));
            setTimeout(e => this.setState({ copiado: '' }), 500)
          }}> {iconCamara} </span>
        
        <i className="material-icons text-success" hidden={ this.state.copiado === ''}>
          { this.state.copiado }
        </i>

        {/* inputs para dimensiones */}
        <div className="form-inline">
          alto: {nbsp(3)}
          <input 
            onChange={ e => this.setState({ height: e.target.value })}
            value={this.state.height}
            type="number"
            className="form-control" 
            min="1" max="100" />%
          {nbsp(10)}
          ancho: {nbsp(3)}
          <input 
            onChange={ e => this.setState({ width: e.target.value })}
            value={this.state.width}
            type="number" 
            className="form-control" 
            min="1" max="100" />%
        </div>

        {/* input para el src de la imagen */}
        <input 
          onChange={e => this.setState({ src: e.target.value })} 
          className="form-control"
          placeholder='src'/>
      </div>
    )
  }
}

export class BlogPre extends Component {
  state = {
    es6Code: '',
    copiado: ''
  }

  escape = (html) => {
    var escaped = html    
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
    return escaped
  }

  render() {
    let css = { padding: 5, backgroundColor: 'lightred' }
    return (
      <div style={css}>
        escribir código {nbsp(10)}
        {/* botones copiar */}
        <span
          onClick={e => {
            let txt = `<pre class="alert-success">\n${this.state.es6Code}\n</pre>\n`
            navigator.clipboard.writeText(txt).then(e => this.setState({ copiado: 'done_outline' }));
            setTimeout(e => this.setState({ copiado: '' }), 500)
          }}> {iconCamara} </span>

        <i className="material-icons text-success" hidden={this.state.copiado === ''}>
          {this.state.copiado}
        </i>

        {/* pegar código aquí */}
        <textarea 
          onChange={e => {
            this.setState({ es6Code: this.escape(e.target.value) })
          }} 
          rows="10"
          className="form-control"/>

      </div>
    )
  }
}

export class BlogLink extends Component {
  state = { url: '', texto: '', copiado: '' }
  
  render() {    
    let css = { padding: 20, backgroundColor: '', width: '100%' }

    return (
      <div style={css}>
        crear HTML para Link {nbsp(10)}
        {/* 2: la cámara copia en clipboard el html final para pegar en el blog;
        y pone abajo icono done_outline durante 30 segundos, para que el UI diga que se copió bien */}
        <span
          onClick={e => {
            let txt = `<a href="${this.state.url}" target="_blank" />\n\t${this.state.texto}\n</a>`
            navigator.clipboard.writeText(txt).then(e => this.setState({ copiado: 'done_outline' }));
            setTimeout(e => this.setState({ copiado: '' }), 500)
          }}> {iconCamara} </span>

        <i className="material-icons text-success" hidden={this.state.copiado === ''}>
          {this.state.copiado}
        </i>


        {/* 1: los inputs para url y texto del enlace */}
        <input 
          onChange={e => this.setState({ url: e.target.value })} 
          className="form-control"
          placeholder='url '/>
        <input 
          onChange={e => this.setState({ texto: e.target.value })} 
          className="form-control"
          placeholder='texto del enlace'/>

      </div>
    )
  }
}

export class BlogImg extends Component {
  state = { src: '', copiado: '', height: 50, width: 50 }

  render() {
    let css = { padding: 20, backgroundColor: '', width: '100%' }

    return (
      <div style={css}>
        crear HTML para Img {nbsp(5)}
         {/* 2: la cámara copia en clipboard el html final para pegar en el blog;
        y pone abajo icono done_outline durante 30 segundos, para que el UI diga que se copió bien */}
        <span
          onClick={e => {
            let txt = `<br>\n<a href="${this.state.src}" target="_blank">\n<img src="${this.state.src}" height="${this.state.height}%"  width="${this.state.width}%" />\n</a>`
            navigator.clipboard.writeText(txt).then(e => this.setState({ copiado: 'done_outline' }));
            setTimeout(e => this.setState({ copiado: '' }), 500)
          }}> {iconCamara} </span>
        
        <i className="material-icons text-success" hidden={ this.state.copiado === ''}>
          { this.state.copiado }
        </i>


        {/* pieza 1 el input para escribir la url de la imagen */}
        {br(1)}

        {/* inputs para dimensiones */}
        <div className="form-inline">
          alto: {nbsp(3)}
          <input 
            onChange={ e => this.setState({ height: e.target.value })}
            value={this.state.height}
            type="number"
            className="form-control" 
            min="1" max="100" />%
          {nbsp(10)}
          ancho: {nbsp(3)}
          <input 
            onChange={ e => this.setState({ width: e.target.value })}
            value={this.state.width}
            type="number" 
            className="form-control" 
            min="1" max="100" />%
        </div>

        {/* input para el src de la imagen */}
        <input 
          onChange={e => this.setState({ src: e.target.value })} 
          className="form-control"
          placeholder='src'/>

       
        {/* 3: la caja donde vemos lo que se va a copiar */}
        {/* <pre 
          dangerouslySetInnerHTML={ { __html: `&lt;img src="${this.state.src}" height="100%"  width="100%" /&gt;` }}
          title="click para copiar"
          style={{ padding: 20}}/>
        
        <pre> {JSON.stringify(this.state, undefined, 2)} </pre> */}
      </div>
    )
  }
}

export class BlogLista extends Component {
  state = { items: [], newItem: '', copiado: '', tipoLista: '', ordenada: 'ol', 
    newI: { text: '', class: '' },
    newItems: []
  }

  getHtml = () => {
    let html = `<${this.state.ordenada} type="${this.state.tipoLista}">\n`
    this.state.items.forEach( item => html += `<li class=""> ${item} </li>\n` )
    html += `</${this.state.ordenada}>\n`    
    return html
  }

  render() {
    let css = {padding: 20}
    return (
      <div style={css}>
        {/* decir en algún sitio para qué sirve este componente */}
        crear HTML para OL {nbsp(5)}

        {/* 3.- los iconos de copiar como en los componentes anteriores */}
        <span
          onClick={e => {
            let txt = this.getHtml()
            navigator.clipboard.writeText(txt).then(e => this.setState({ copiado: 'done_outline' }));
            setTimeout(e => this.setState({ copiado: '' }), 500)
          }}> {iconCamara} </span>
        
        <i className="material-icons text-success" hidden={ this.state.copiado === ''}>
          { this.state.copiado }
        </i>


        {/* los checkbox para cambiar lo tipos de lista */}
        {br(1)}
        letras: {nbsp(3)}
        <input 
          onChange={e => this.setState({ tipoLista: this.state.tipoLista === '' ? 'a' : '' }) }          
          type="checkbox"/>
        {nbsp(5)}
        ordenada: {nbsp(3)}
        <input 
          onChange={e => this.setState({ ordenada: this.state.ordenada === 'ol' ? 'ul' : 'ol' }) }
          checked={this.state.ordenada === 'ol' }
          type="checkbox"/>

        {/* 1.- el input para ir escribiendo el texto de los items */}
        {br(1)}
        <div className="form-inline">
          <input 
            id="newItem"
            onChange={ e => this.setState({ newItem: e.target.value })}
            value={this.state.newItem}
            className="form-control"
            size="50"
            placeholder="escribe el item de lista"/>
                    
          {/* 2.- el icono para añadir el item */}
          <button onClick={e => {
            let newList = this.state.items
            newList.push(this.state.newItem)
            this.setState({ items: newList, newItem: '' }, e => console.log(this.getHtml()))
            document.getElementById('newItem').focus()
          }}
          style={{ padding: 0.5, height: 40, width: 40 }}
          className="btn btn-sm">{iconNuevo}</button>
        </div>


        {/* 4.- las cajas de preview según <ol> o <ul>*/}    
        <div hidden={this.state.ordenada === 'ul'}>
          <ol type={this.state.tipoLista}>
          {this.state.items.map( (item, index) => <li key={index}> {item} </li> )}    
          </ol>
        </div>
        <div hidden={this.state.ordenada === 'ol'}>
          <ul>
          {this.state.items.map( (item, index) => <li key={index}> {item} </li> )}    
          </ul>
        </div>
      </div>
    )
  }
}