import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { nbsp, br,  findSort, updateOne, insertOne, stitch, reactLink } from '../index';
import { ObjectId } from 'bson';


const dbname = 'domingo';
const collection = 'alumnos';
const iconSearch = (<i className="material-icons" title="ir a Git"> search </i>) 
const iconSave = (<i className="material-icons " title="guardar"> save </i>) 
const iconNotas = (<i className="material-icons" title="gestionar cursos"> how_to_reg </i>) 
// const iconBug = (<i className="material-icons" title="depurar"> bug_report </i>)
// const iconPonerNotas = (<i className="material-icons" title="poner notas"> create_new_folder </i>)
const iconAddTarget = (<i className="material-icons" title="gestión de objetivos"> sentiment_satisfied_alt </i>)
// const iconCloseModal = (<i className="material-icons" title="cerrar"> block </i>)
const iconEquis = (<i
  className="material-icons"
  style={{ fontSize: 30, color: 'black', backgroundColor: 'white' }}
  title="cerrar"> close </i>)    
// const iconOk = (<i className="material-icons " title="ok"> done_outline </i>)
// const iconDelete = (<i className="material-icons " title="eliminar objetivo"> delete </i>)
// const iconLapiz = (<i className="material-icons " title="editar"> create </i>)


export class Alumno extends Component {  

  constructor(props){
    super(props);
    this.state = { 
      // declarar arrays vacíos para que no protesten los .map() iniciales
      doc: { autor: stitch.auth.user.id, notas: [], devTargets: [], cursosAsignados: [] },
      resp: { modifiedCount: 0, insertedId: null },
      notasOcultas: true, // notas del alumno oculto, sólo yo puedo abrirlas
      jsonOculto: true, // formulario de Debug oculto
      formUfOculto: true, // formulario de nueva UF oculto
      formAlumnoOculto: true, // formulario de Alumno oculto
      newUf: { ufName: '', clase: 0, teorico: 0, practico: 0 }, // esquema para añadir notas de una UF
      saveDoc: () => {} // [1] this.saveDoc puede ser this.insertDoc() o this.updateDoc()
    }
  }  

  componentDidMount = n => {  
    // se trae el Alumno si en la url no recibe "nuevo"
    // [1] y asigna this.saveDoc() para añadir o actualizar (es una opción de tantas!)
    if( this.props.match.params.id === 'nuevo' ){  this.setState({ saveDoc: this.insertDoc})  }
    else { this.getDoc();  this.setState({ saveDoc: this.updateDoc }) }    
  }

  getDoc = n => {
    findSort({ 
      db: dbname, 
      collection: collection,   
      find: { _id: new ObjectId(this.props.match.params.id) },
      sort: {}
    })
    .then( data => { 
      console.log(data); 
      this.setState({ doc: {...this.state.doc, ...data[0]}  } ) // data[0] porque findSort() devuelve siempre un array
      // leer(
      //   `${this.state.doc.nombre} ...
      //   Objetivos ${this.state.doc.devTargets.length} ...`        
      // )
    }); 
  }


  getElNew = n => {
    findSort({ 
      db: dbname, 
      collection: collection,   
      find: { _id: new ObjectId( this.state.resp.insertedId ) },
      sort: {}
    })
    .then( data => { 
      console.log(data); 
      this.setState({ doc: {...this.state.doc, ...data[0]}  } ) // data[0] porque findSort() devuelve siempre un array      
      // leer(
      //   `${this.state.doc.nombre} ...
      //   Objetivos ${this.state.doc.devTargets.length} `        
      // )

    }); 
  }

  updateDoc = n => {
    // actualiza un alumno en Mongo necesita un "_id",
    // que sólo tiene si se ha llamado por URL con "_id" de Mongo
    let params = { db: dbname, collection: collection, doc: this.state.doc }    
    updateOne(params)
    .then( resp => { 
      this.setState({ resp: { modifiedCount: this.state.resp.modifiedCount + 1} }) // para que el user sepa si se ha grabado o no se usa this.state.resp
    })    
  }

  insertDoc = n => {
    // no tiene "_id" porque viene "nuevo" en la URL así que Mongo inserta sin problemas
    let params = { db: dbname, collection: collection, doc: this.state.doc }    
    insertOne(params).then( resp => { 
      this.setState({ resp: { insertedId: resp.insertedId.toString() } }, this.getElNew);
      this.setState({ saveDoc: this.updateDoc })      
    })    
  }

  changeInput = e => {
    // la clásica changeInput, se puede mejorar para que maneje también <input type="number">
    // está hecho en gestión de notas del Alumno
    this.setState({ doc: {...this.state.doc, [e.target.id]: e.target.value} })
  }

  botoneraLinea1 = e => {
    return (
      <div>
        <span 
        onClick={ this.state.saveDoc }     
        title="guardar cambios"> 
          { iconSave } 
        </span>

        {nbsp(3)}
        <a 
          href={this.state.doc.git} 
          target="_blank" 
          style={ {color: 'grey'} }
          title="ir a git"
          rel="noopener noreferrer"> 
          {iconSearch} 
        </a>          
          
        {nbsp(6)}         
        <span 
          hidden={this.state.resp.modifiedCount === 0} 
          title="modifiedCount desde el server"
          className="circle">
          {this.state.resp.modifiedCount}
        </span>  

      </div>      
    )
  }

  forminputsAlumno = e => {
    return (
      <div className="form-inline" hidden={this.state.formAlumnoOculto}>
      <input 
        id="nombre"
        onChange={ this.changeInput } 
        value={this.state.doc.nombre}        
        placeholder="Nombre"
        className="form-control" />

      <input 
        id="email"
        title={this.state.doc.email}
        onChange={ this.changeInput } 
        value={this.state.doc.email}        
        placeholder="Email"
        className="form-control" />

      <input 
          id="git"
          onChange={ this.changeInput } 
          title={this.state.doc.git}
          value={this.state.doc.git}        
          placeholder="Git"
          className="form-control" />                
    </div>    
    )
  }

  formAlumno = n => {    
    // está razonablemente complicada,
    // a punto de refactorizar    
    return (
      <div>         
        { /* botones de acción primera línea */}   
         {this.botoneraLinea1()}
 
        { /* resumen datos alumno y toggle del formulario  */  }
        <div 
          onClick={e => this.setState({ formAlumnoOculto: !this.state.formAlumnoOculto}) }
          className="cpointer">
          <span className="nombre-alumno"> { this.state.doc.nombre || 'alumno sin datos, click para editar' } </span> <br />
          <small> { this.state.doc.email } </small> <br />
          <small> { this.state.doc.git } </small> <br />
        </div>
        {/* inputs de nombre, email, etc. */}
        {this.forminputsAlumno()}
      </div>
    )
  }


  



  render() {
    // refactorizar en Componentes    
    
    return (
      <div className="container">
        <br />
        {  this.formAlumno()  }
        <br />
        <Link to={`/targets/${this.state.doc._id}`}  style={reactLink}> {iconAddTarget} </Link>

        {nbsp(3)}<small>objetivos</small>

        <ListTargetsRO 
          targets={this.state.doc.devTargets} 
          style={{float: 'right'}}
        />

        {br(1)}

        <Link to={`/notas/${this.state.doc._id}`}  style={reactLink}> {iconNotas} </Link>
        {nbsp(3)}
        <small>cursos</small>
        {br(1)}

        {this.state.doc.cursosAsignados.map( (curso, i) => <li key={i}> {curso.titulo} </li>)}

        {/* sólo debug */}
        <pre hidden={this.state.jsonOculto}> {JSON.stringify(this.state, undefined, 2)} </pre>

    </div>
    )
  }

} // end class Alumno


export class ListTargetsRO extends Component {  
  constructor(props){
    super(props);
    this.state = { 
      htmlOculto: true
    }
  }

  changeHtmlOculto = index => {
    // this.setState({ htmlOculto: !this.state.htmlOculto})
    document.querySelector(`#modal${index}`).style = 'display: block'
  }

  showTarget = (target, index) => {
    return (<div key={index} style={{ padding: 8}}>
      <small className="circle" title="dificultad"> <small> {target.gradoDificultad} </small>  </small>
      
      {nbsp(2)}      
      { target.archivado ? 
        (<strike title="archivado"><small onClick={e => this.changeHtmlOculto(index) }> {target.titulo} </small></strike>) : 
        (<span><small onClick={e => this.changeHtmlOculto(index) }> {target.titulo} </small></span>)
      }

      {nbsp(2)}      
      { target.fechaInicio ? 
        (<small> {target.fechaInicio} </small>) : 
        (<small> sin fecha </small>)
      }

      
      <div id={`modal${index}`} className="modal2" name="modal">       
      {nbsp(20)}

      <span 
        onClick={e => document.querySelector(`#modal${index}`).style = 'display: none'}
        style={{ color: 'black', backgroundColor: '', position: 'relative', top: 30, left: 15}}> 
        {iconEquis} 
      </span>

        <div className="modal2-content">
          <div dangerouslySetInnerHTML={{__html: target.html ? target.html : 'sin descripcón del proyecto'}} />
        </div>
      </div>
    </div>)
  }

  render(){
    return (<div>

      {nbsp(3)}
      {br(1)}
      { this.props.targets.map( this.showTarget ) }      
    </div>)
  }

} // end class ListTargetsRO