import React, { Component } from 'react'

export default class ShowTablasMultiplicar extends Component {    

  render() {
    let padding = { padding: 50 }
    let numeros = [1,2,3,4,5,6,7,8,9,10];
    return (
      <div style={padding}>
        <h3> tabla del {this.props.match.params.numero} </h3>
        { 
          numeros.map( (num, i) => <ShowFila key={i} tablaActual={this.props.match.params.numero} numero={num} />  ) 
        }      
      </div>
    )
  }
}

class ShowFila extends Component {

  spanTablaActual = () => {
    let style = {fontSize: 15, color: 'green'}
    return <span style={style}> {this.props.tablaActual} </span>
  }

  spanFilaActual = () => {
    let style = {fontSize: 15, color: 'blue'}
    return <span style={style}> {this.props.numero} </span>
  }
  
  spanResultado = () => {
    let style = { fontSize: 22, color: 'red', align: 'right'}
    let resultado = this.props.tablaActual * this.props.numero
    return <span style={style}> {resultado} </span>
  }
    
  render() {
    let style = { padding: 2 }
    return (
      <div style={style}>
        {this.spanTablaActual()} 
        <small>por</small> 
        {this.spanFilaActual()} 
        <small> = </small>
        {this.spanResultado()}
      </div>
    )
  }
}



