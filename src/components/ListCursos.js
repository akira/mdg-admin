import React from 'react';
import { br, nbsp, findSort } from '../index';

const dbname = 'domingo';
const collection = 'modelosCurso';

export default class ListCursos extends React.Component {
  constructor(props){
    super(props)
    this.state = { 
      modelosCurso: []  }
  }

  componentDidMount(){
    findSort({db: dbname, collection: collection, find: {}, sort: {} })
    .then( data => this.setState({ modelosCurso: data }) );
  }

  rederCurso = (curso,index) => {
    return (<div key={index} className="container"> 
      {curso.codigoBOE} {nbsp(10)} {curso.titulo}
      {br(1)}
      {curso.modulos.map( this.renderModulo )}
      <hr />
    </div>)    
  }

  renderModulo = (modulo, index) => {
    return (<div key={index} className="container">
      <small key={modulo.id}> {modulo.titulo} </small> 
      { modulo.ufs.map( this.renderUf ) }            
    </div>)
  }

  renderUf = (uf, index) => {
    return (<div key={index} className="container">
      <small key={uf.id}> {uf.titulo} </small> 
    </div>)
  }
  render(){
    return (<div>
      { this.state.modelosCurso.map( this.rederCurso ) }      
    </div>)
  }
}
