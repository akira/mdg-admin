import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { br, nbsp, reactLink, findSort, updateOne } from '../index';
import { ObjectId } from 'bson';

const dbname = 'domingo';
const collection = 'alumnos';
const cursos = 'modelosCurso';
const iconSubir = (<i 
  className="material-icons" 
  style={{ fontSize: 15 }}
  title="subir nota"> thumb_up </i>)
const iconBajar = (<i 
  className="material-icons" 
  style={{ fontSize: 15 }}
  title="bajar nota"> thumb_down </i>)
const iconOk = (<i 
    className="material-icons"   
    title="guardar cambios"> check_circle_outline </i>)
const iconAsignarCurso = (<i 
  className="material-icons"
  title="asignar a este alumno"> add_shopping_cart </i>)
const iconBack = (<i
  className="material-icons"
  style={{ fontSize: 25 }}
  title="volver al alumno"> replay </i>)

export default class Notas extends Component {

  constructor(props){
    super(props);
    this.state = { 
      alumno: { 
        cursosAsignados: [],
        devTargets: [] 
      },
      modelosCurso: [],
      cursoSeleccionado: {},
      debug: true,
      cambiosSinGuardar: 0,
      modifiedCount: 0,      
    }
  }  

  componentDidMount = e => { this.getCursos(); this.getDoc() }

  getDoc = n => {
    findSort({ 
      db: dbname, 
      collection: collection,   
      find: { _id: new ObjectId(this.props.match.params.alumno) },
      sort: {}
    })
    .then( data => { 
      console.log(data); 
      this.setState({ alumno: {...this.state.alumno, ...data[0]}  } ) // data[0] porque findSort() devuelve siempre un array
    }); 
  }

  getCursos = n => {
    findSort({ 
      db: dbname, 
      collection: cursos,   
      find: { },
      sort: { titulo: 1 }
    })
    .then( data => { 
      console.log(data); 
      this.setState({ modelosCurso: data } ) 
    }); 
  }

  getCurso = id => {
    console.log('_id del curso', id)
    findSort({ 
      db: dbname, 
      collection: cursos,   
      find: { _id: new ObjectId(id) },
      sort: {}
    })
    .then( data => { 
      console.log('getCurso()', data); 
      this.setState({ cursoSeleccionado: {...data[0]} }) // data[0] porque findSort() devuelve siempre un array
    });     
  }

  saveAlumno = e => {
    updateOne({db: dbname, collection: collection, doc: this.state.alumno})
    .then( resp => { this.setState({ modifiedCount: this.state.modifiedCount + 1, cambiosSinGuardar: 0}); console.log('resp', resp) })
    .catch( err => console.log('err', err))
  }

  // exportada
  updateCurso = (curso, index) =>{
    let cursos = this.state.alumno.cursosAsignados;
    cursos[index] = curso;
    this.setState({ 
      alumno: {...this.state.alumno, cursosAsignados: cursos},
      cambiosSinGuardar: this.state.cambiosSinGuardar + 1
    })
  }

  // renders
  renderSelectCursos = e => {
    let style = {float: 'right'}
    let formInline = "form-inline"
    let formControl = 'form-control'

    let changeSelect = e => { this.getCurso(e.target.value) }
    let botonAsignarCurso = e => {
      let newCursosAsignados = this.state.alumno.cursosAsignados || [];
      newCursosAsignados.unshift(this.state.cursoSeleccionado); // añadimos al inicio
      this.setState({
        alumno: {...this.state.alumno, cursosAsignados: newCursosAsignados },
        cambiosSinGuardar: this.state.cambiosSinGuardar + 1
      })
    }


    return (<div style={style}>
      <div className={formInline}>
      <select 
        onChange={changeSelect}
        className={formControl}>  
          <option defaultValue> selecciona curso </option>
          {this.state.modelosCurso.map( this.renderOptionSelectCursos )}
      </select>
      {nbsp(5)}
      <span onClick={botonAsignarCurso}>{iconAsignarCurso}</span>
      </div>

      </div>)
  }

  renderOptionSelectCursos = curso => {
    let key = curso._id.toString();
    let value = curso._id
    return (
    <option key={key} value={value}>
      {curso.titulo}
    </option>)
  }

  renderDebug = n => {
    return (
      <div className="container">
        <hr />
        debug <input 
          type="checkbox"
          onChange={e => {
            this.setState({ debug: !this.state.debug })
          }}
        />
        <pre hidden={this.state.debug}> {JSON.stringify(this.state, undefined, 2)} </pre>
      </div>
    )
  }

  renderCursosAsignados = e => {
    return (<div>
      {this.state.alumno.cursosAsignados.map( this.renderCursoAlumno )}
    </div>)
  }

  renderCursoAlumno = (curso, index) => {
    let key = curso._id.toString();
    // let titulo = curso.titulo;
    return ( <Curso key={key} curso={curso} index={index} updateCurso={this.updateCurso} /> );
  }

  render() {
    return (<div className="container">
      <Link to={`/alumno/${this.state.alumno._id}`} style={reactLink}>  {iconBack} </Link>   

      <h4> { this.state.alumno.nombre} </h4>
      <span hidden={this.state.modifiedCount === 0} className="circle"> {this.state.modifiedCount} ok del server </span>
      <span 
        onClick={ this.saveAlumno }
        hidden={false}> {iconOk} </span>
      {this.renderSelectCursos()}

      {br(1)} proyectos: <span className="circle"> {this.state.alumno.devTargets.length || 0} </span>

      

      <hr />
      {this.renderCursosAsignados()}
      {/* { this.renderDebug() } */}
    </div>)
  }
} // end Class Notas

class Curso extends Component {
  state = {
    curso: this.props.curso,
    hideDetails: true,
    cambiosSinGuardar: 0
  }

  // exportada abajo
  updateModulo = (modulo, index) => {
    let modulos = this.state.curso.modulos;
    modulos[index] = modulo;
    this.setState({ 
      curso: { ...this.state.curso, modulos: modulos},
      cambiosSinGuardar: this.state.cambiosSinGuardar + 1 
    }, e => { this.props.updateCurso(this.state.curso, this.props.index) });
  }
  
  render() {
    let clickSpanTitulo = e => { this.setState({ hideDetails: ! this.state.hideDetails }) }
    // let clickEliminarCurso = e => { console.log('clickEliminarCurso') }
    
    return (
      <div className="container">
        <span onClick={clickSpanTitulo} className="cpointer" style={{fontSize: 25}}> {this.state.curso.titulo} </span>        
        {br(1)}
        <small> {this.state.curso.modulos.length} módulos </small>        

        {/* <span 
          onClick={e => { 
            this.props.updateCurso(this.state.curso, this.props.index);
            this.setState({ cambiosSinGuardar: 0 })
          }}
          hidden={this.state.cambiosSinGuardar === 0}> {iconOk}  grabar este curso </span> */}

        <div hidden={this.state.hideDetails}>
          {this.state.curso.modulos.map( (modulo, index) => <Modulo key={modulo.id} modulo={modulo} index={index} updateModulo={this.updateModulo} /> )}
        </div>
      </div>
    )
  }
}

class Modulo extends Component {
  state = {
    modulo: this.props.modulo,
    ufsHidden: true,
    cambiosSinGuardar: 0
  }

  updateAsignatura = (asignatura, index) => {
    // cambiar o borrar state.modulo según lo que se recibe:
    let origAsignaturas = this.state.modulo.ufs;
    origAsignaturas[index] = asignatura
    this.setState({ 
      cambiosSinGuardar: this.state.cambiosSinGuardar + 1,
      modulo: {...this.state.modulo,  ufs: origAsignaturas} 
    },
    e => { this.props.updateModulo(this.state.modulo, this.props.index) })
  }


  render(){     
    let clickTituloModulo = e => {
     this.setState({ ufsHidden: ! this.state.ufsHidden }) 
    }    

    return ( 
    <div className="container"> 

      {/* <span 
        onClick={e => {
          this.props.updateModulo(this.state.modulo, this.props.index);
          this.setState({ cambiosSinGuardar: 0 })
        }}
        hidden={this.state.cambiosSinGuardar === 0 }        
        className="cpointer">
       {iconOk} actualizar módulo
      </span> */}

      

      <span 
        onClick={clickTituloModulo}
        style={{fontSize:20}}
        className="cpointer"> {this.state.modulo.titulo} </span>

      <small> {this.state.modulo.ufs.length} asignaturas </small>        

      <div hidden={this.state.ufsHidden} className="container">
        {this.state.modulo.ufs.map( (uf, index) => 
          <Asignatura 
            key={uf.id} 
            uf={uf} 
            index={index} 
            updateAsignatura={this.updateAsignatura} 
          /> )}
      </div>
      {/* el módulo:
      <pre> {JSON.stringify(this.state.modulo, undefined, 1)} </pre> */}

    </div>
  )}
}

class Asignatura extends Component {
  state = {  
    asignatura: this.props.uf,
    hideAsignatura: true
  }

  
  render(){

    let inputNotaClase = e => {
      let nota = this.state.asignatura.notaClase;
      return (
        <span>
          <span 
            onClick={e => {
              this.setState({ 
                asignatura: { ...this.state.asignatura, notaClase: nota += 0.5 } 
              },
              e => {
                this.props.updateAsignatura(this.state.asignatura, this.props.index )
              });
            }}

          >{iconSubir}</span>
          {nbsp(3)}
          <span 
            onClick={e => {
              this.setState({ 
                asignatura: { ...this.state.asignatura, notaClase: nota -= 0.5 } 
              },
              e => {
                this.props.updateAsignatura(this.state.asignatura, this.props.index )
              });
            }}

          >{iconBajar}</span>
         {nbsp(3)} clase: {this.state.asignatura.notaClase} 

        </span>
      )
    }

    let inputNotaTeorico = e => {
      let nota = this.state.asignatura.notaTeorico;
      return (
        <span>
          <span 
            onClick={e => {
              this.setState({ asignatura: { ...this.state.asignatura, notaTeorico: nota += 0.5 } },
              e => {
                this.props.updateAsignatura(this.state.asignatura, this.props.index )
              });
            }}

          >{iconSubir}</span>
          {nbsp(3)}
          <span 
            onClick={e => {
              this.setState({ asignatura: { ...this.state.asignatura, notaTeorico: nota -= 0.5 } },
              e => {
                this.props.updateAsignatura(this.state.asignatura, this.props.index )
              });
            }}

          >{iconBajar}</span>
         {nbsp(3)} teórico: {this.state.asignatura.notaTeorico} 

        </span>        
      )
    }


    let inputNotaPractico = e => {
      let nota = this.state.asignatura.notaPractico;
      return (
        <span title="nota del ejercicio práctico">
          <span 
            onClick={e => {
              this.setState({ asignatura: { ...this.state.asignatura, notaPractico: nota += 0.5 } },
              e => {
                this.props.updateAsignatura(this.state.asignatura, this.props.index )
              });
            }}

          >{iconSubir}</span>
          {nbsp(3)}
          <span 
            onClick={e => {
              this.setState({ asignatura: { ...this.state.asignatura, notaPractico: nota -= 0.5 } },
              e => {
                this.props.updateAsignatura(this.state.asignatura, this.props.index )
              });
            }}

          >{iconBajar}</span>
         {nbsp(3)} práctico: {this.state.asignatura.notaPractico} 

        </span>        
      )
    }


    return(
      <div className="container">
        <div 
          onClick={ e => this.setState({hideAsignatura: !this.state.hideAsignatura}) }
          className="cpointer"
          style={{color: 'black'}}
          title="título de la asignatura"> {this.state.asignatura.codigo} {this.state.asignatura.titulo} </div>     
        <div hidden={this.state.hideAsignatura} className="container" style={{backgroundColor: 'white', padding: 10}}>
          {inputNotaClase()} 
          {nbsp(2)} {inputNotaTeorico()} 
          {nbsp(2)} {inputNotaPractico()} 
          {br(1)}
          media: { (this.state.asignatura.notaPractico + this.state.asignatura.notaTeorico) / 2 }
        </div>        
        
        
        {/* aquí state:
        <pre> {JSON.stringify(this.props.alumno, undefined, 2)} </pre> */}
      </div>
    )
  }
}
