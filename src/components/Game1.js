import React from 'react';
import { diaSemana } from '../index'

const iconRun = (<i className="material-icons" title="correr"> directions_run </i>) 
const iconCrono = (<i className="material-icons" title="comenzar crono"> alarm </i>) 


export class Circulo extends React.Component {
  
  state = { 
    name: this.props.name,
    // width: 100, height: 100,
    cx: 50, cy: 50, r: 20,
    // stroke: 'black', strokeWidth: 2,
    fill: 'yellow',
    endgame: false,
  }

  toggleColor = e => {
    let color = this.state.fill === 'yellow' ? 'green' : 'yellow';
    this.setState({ fill: color }, e => { this.props.sendMessage(this.state) });
  }
  
  render(){
    return (
    <div className="container">

      <span onClick={ e => {
          let gameOver = null;
          if ( gameOver ) { clearInterval(gameOver) }
          else { gameOver = setTimeout( e => { this.setState({ endgame: true }) }, 5000) }
      }}> {iconCrono} </span>

      <div hidden={this.state.endgame}>
      <span onClick={ e => { 
          this.setState({ cx: this.state.cx + 10 });            
        }}
        className="btn-sm"> {iconRun} </span>
      </div>



      <svg width="800" height="100">
        <circle 
          onClick={ this.toggleColor }
          {...this.state}>
        </circle>
        <text x={this.state.cx} y="50" fontFamily="Verdana" fontSize="13px">
           {this.state.name}: {this.state.cx - 50 } mts ({(this.state.cx - 50 ) / 10} clicks)
        </text>          

      </svg> 
    </div>
    )  
  }
}


 
export default class Game1 extends React.Component {
  state = {     
    message: '',
    circulos: [ {name: 'Jugador 1'}, {name: 'Jugador 2'}]
  }

  receiveMessage = message => {
    this.setState({ message: message })
  }

  render(){
    return (<div className="container">
      <h5> hoy es {diaSemana()}, quién hará click más rápido? </h5>
      <p> tienes 5 segundos para correr todo lo que puedas</p>
      <br />
      { this.state.circulos.map( (c,i) => <Circulo key={i} {...c} sendMessage={ this.receiveMessage } />  ) }
      <hr />
      {/* <pre>
      {JSON.stringify(this.state.message, undefined, 2)}
      </pre> */}
      <button className="btn btn-info" 
        onClick={ e => { 
        let circulos = this.state.circulos;        
        this.setState({ circulos: []}, e => { this.setState({ circulos: circulos })})
      }}> otro juego </button>
    </div>)
  }
}
