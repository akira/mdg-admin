import React, { Component } from 'react'
import { findSort, nbsp, br } from '../index'
import { ActuacionSchema } from './GestionSala'
import { Link } from 'react-router-dom'

export class Actuaciones extends Component {
  state = {
    bolos: [],
    totales: { }
  }
  componentDidMount(){
    findSort({db: 'promusic', collection: 'actividad', find: {}, sort:{ fechaPrevista: 1,  } })
    .then( data => { 
      this.setState({ bolos: data }, () => this.saldoEmpresa() )      
    })
  }

  render() {
    let css = { padding: 20}
    let cssLink = { color: 'grey', textDecoration: 'none'}
    let cssSaldo = this.state.totales.beneficios - this.state.totales.costes < 0 ? {color: 'red', fontSize: 20} : {color: 'green', fontSize: 20}
    let costes = <span title="gastos" style={{color: 'red'}}> {parseFloat(this.state.totales.costes).toLocaleString()} € </span>
    let beneficios = <span title="ingresos" style={{color: 'green'}}> {parseFloat(this.state.totales.beneficios).toLocaleString()} € </span>
    let saldo = <span style={cssSaldo}> {parseFloat(this.state.totales.beneficios - this.state.totales.costes).toLocaleString()} € </span>    
    let linkToNewBolo = <Link to={`/actividad/new`} style={cssLink} > nuevo bolo </Link>
    let linkToHome = <Link to={`/promhome`} style={cssLink} > Home </Link>

    return (
      <div style={css}>
        {this.state.bolos.length} bolos {br(1)}
        {beneficios} - {costes} = {saldo} {br(1)}
        {linkToNewBolo} {nbsp(5)} {linkToHome}
        {br(2)}
        <div>
          {this.state.bolos.map( (b, i) => <InfoBolo key={i} {...b} /> )}
        </div>
        
      </div>
    )
  }

  saldoEmpresa(){
    let costes = 0
    let beneficios = 0
    this.state.bolos.forEach( bolo => {
      let actuacion = Object.assign(new ActuacionSchema(), bolo) 
      costes += actuacion.costes()
      beneficios += actuacion.beneficios()
    })
    this.setState({ totales: {...this.state.totales, costes: costes, beneficios: beneficios, saldo: beneficios - costes } })
  }
}

export class InfoBolo extends Component {
  state = { costes: 0 }
  componentDidMount(){
    let bolo = Object.assign(new ActuacionSchema(), this.props)
    this.setState({ costes: bolo.costes(), beneficios: bolo.beneficios(), saldo: bolo.beneficios() - bolo.costes() })
  }

  render() {
    let css = {padding: 10, float: 'left', margin: 5, border: '1px solid'}
    let nombre = <span className="cpointer"> <Link to={`/actividad/${this.props._id}`} style={{textDecoration: 'none', color: 'black'}}> {this.props.producto.nombre} </Link> </span> 
    let fecha = this.props.fechaPrevista
    let sala = this.props.sala.nombre
    let ciudad = this.props.sala.ciudad
    let beneficios = <span style={{color: 'grey'}} title="beneficio"> {parseFloat(this.state.beneficios).toLocaleString()} €</span> 
    let costes = <span style={{color: 'grey'}} title="coste"> {parseFloat(this.state.costes).toLocaleString()} €</span> 
    let cssSaldo = this.state.beneficios - this.state.costes < 0 ? {fontSize: 20, color: 'red'} : {fontSize: 20, color: 'green'}
    let saldo = <span title="saldo" style={cssSaldo}> {parseFloat(this.state.beneficios - this.state.costes).toLocaleString()} €</span>

    return (
      <div style={css} className="bolo">         
        {fecha} {nbsp(3)}
        {nombre} {br(1)} 
        {sala} {nbsp(1)} <small> {ciudad} </small> {br(1)} 
        caja: {saldo} {br(1)}
        <small> {beneficios} - {costes}  </small>
         
        {/* <pre> {JSON.stringify(this.state, undefined, 2)} </pre> */}
      </div>
    )
  }
}