import React from 'react';
import {nbsp, findSort, distinct } from '../index';
import { Link } from 'react-router-dom'


const dbname = 'domingo', collection = 'blog';

/* iconos de Angular Material*/
const iconBug = (<i className="material-icons" title="depurar"> bug_report </i>)
const iconActivo = (
  <i 
    title="artículo pulicado" 
    className="material-icons text-success"
    style={{ fontSize: '15px' }}>
    wifi 
  </i>); 

const iconInActivo = (
  <i 
    className="material-icons" 
    title="artículo no pulicado" 
    style={{ fontSize: '15px' }}> 
  wifi_off 
  </i>); 

export class FormPost extends React.Component {
  constructor(props){ 
    super(props);
    this.state = { postHidden: true } 
  }
  
  render(){
    return ( 
    <div className="container">
      <h4 className="cpointer" onClick={ e => this.setState({ postHidden: !this.state.postHidden})}> 
      <span>
        { this.props.post.activo ? iconActivo : iconInActivo }
      </span>
      &nbsp; &nbsp;&nbsp;
      <span className="titulo-post-lista"> {this.props.post.titulo} </span>  
      </h4>   
      {/* visualizar si está publicado */}   
      
      <small> {this.props.post.seccion} </small>
      {nbsp(3)}
      <small>edits: {this.props.post.ediciones || 0 } </small>


      {/* artículo renderizado */}   
      <div hidden={this.state.postHidden}>     
        <Link to={`/clase/${this.props.post._id}`} style={ {color: 'grey'} }>
          <i className="material-icons"> create </i>
        </Link>
        <div dangerouslySetInnerHTML={{ __html: this.props.post.html }} />      
      </div>
      <hr className="alert-primary" />
    </div>
    )
  }
}

export class Blog extends React.Component {

  constructor(props){
    super(props);
    this.state = { 
      posts: [],
      post: {},
      secciones: [],
      hideDebug: true
     }
  }

  componentDidMount(){
   this.getPosts();this.selectSecciones();
  }

  getPosts = () => {
    findSort({ db: dbname, collection: collection, find: { }, sort: { activo: 1, titulo: 1 } })
    .then( arrayPosts => this.setState({ posts: arrayPosts }))    
  }

  filterPosts = (e) => {    
    this.setState(
      { seccionActiva: e.target.value }, n => {
        if (this.state.seccionActiva === 'ALL') { this.getPosts(); return false }
        findSort({ db: dbname, collection: collection, find: { seccion: this.state.seccionActiva }, sort: { titulo: 1, activo: 1 } })
        .then( arrayPosts => this.setState({ posts: arrayPosts }))    
      });    
  }

  selectSecciones = n => {
    distinct({db: dbname, collection: collection, field: '$seccion' })
    .then( arraySecciones => {      
      this.setState({ secciones: arraySecciones })
    });
  }
  
  renderSeccion = seccion => {
    return (<option key={seccion._id} value={seccion._id}> {seccion._id} = {seccion.total}   </option>)
  }

  rightButtons = e => {
    return (   
    <div className="text-right container">
      <span onClick={ e => { this.setState({ hideDebug: !this.state.hideDebug }) }}>
        {iconBug}
      </span>
    </div>

    )
  }

  leftButtons = e => {
    return (   
      <div className="container form-inline"> 
      total posts: { this.state.posts.length } &nbsp;&nbsp;&nbsp;      
      <select 
        onChange={ this.filterPosts }
        className="form-control">
        <option value="ALL"> Todos </option>
        { this.state.secciones.map( this.renderSeccion ) }
      </select>

      {/* debug */}
      <pre hidden={this.state.hideDebug}> {JSON.stringify(this.state, undefined, 2)} </pre>
    </div> 

    )
  }  

  render(){
    return (<div>
    { this.rightButtons() }
    { this.leftButtons() }  
    <hr />
    <div>
      { this.state.posts.map( post => <FormPost key={post._id} post={post} /> ) }
    </div>  

    {/* debug */}
    <pre hidden={this.state.hideDebug}> {JSON.stringify(this.state, undefined, 2)} </pre>

    </div>)
  }
}

