import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {nbsp, br, insertOne, updateOne, findSort, stitch} from '../index';
import { ObjectID } from 'bson';


const [db, collection] = ['promusic', 'salas'];

//  Schemas
class SalaSchema {
  constructor(){
    this.nombre = '';
    this.ciudad = '';
    this.pais = '';
    this.direccion = '';
    this.telefono = '';
    this.email = '';
    this.homePage = '';
    this.tarifas = [];
  }
}

export class ActuacionSchema {
  constructor(){
    this.fechaPrevista = ''       // previsto actuar este día
    this.producto = {}            // TODO: el espectáculo debe salir de otra colección
    this.sala = new SalaSchema()  // TODO: también sale de otra colección salas
    this.autor = stitch.auth.user.id
    //  a ver así qué tal:
    this.contabilidad = []  // array de ApunteSchema
  }
  costes(){
    let total = 0
    this.contabilidad.forEach( apunte => total += apunte.coste)
    return total
  }
  beneficios(){
    let total = 0
    this.contabilidad.forEach( apunte => total += apunte.beneficio)
    return total
  }

}

class ApunteSchema {
  // {concepto: 'micrófono', coste: 0, beneficio: 0 }    
  constructor(obj){
    this.concepto = obj && obj.concepto ? obj.concepto : '';
    this.coste = obj && obj.coste ? obj.coste : 0;
    this.beneficio = obj && obj.beneficio ? obj.beneficio : 0;    
  }
}

// API:
// programar actuación: <Link to={`/actividad/nueva`} > programar Bolo </Link>
// gestionar actuación: <Link to={`/actividad/${_id}`} > gestionar Bolo </Link>
export class GestionActividad extends Component {

  state = {
    bolo: new ActuacionSchema(),
    salas: [],
    save: null,
    newApunte: new ApunteSchema({concepto: '', coste: 0, beneficio: 0}),
    hideInputsNewApunte: true,
    hideInputsBolo: true
  }
  // Mongo:
  getSalas(){
    findSort({ db: 'promusic', collection: 'salas', find: {}, sort: {ciudad: 1, nombre:1 } })
    .then( array => 
      this.setState({ salas: array }) 
    )
  }

  aMongo = e => {
    let api = {db: db, collection: 'actividad', doc: this.state.bolo }  
    this.state.save(api).then( resp => console.log(resp))    
  }

  componentDidMount(){
    // obtenemos salas:
    this.getSalas();
    // si el parámetro de url tiene 24 chars, parece un _id de Mongo else será nuevo:    
    if (this.props.match.params.id.length === 24) { 
      // recuperar la sala de Stitch mediante findSort()
      let api = { db: 'promusic', collection: 'actividad', find: {_id: new ObjectID(this.props.match.params.id) }, sort: {} }
      findSort(api).then( array => {                 
        this.setState({ bolo: array[0], save: updateOne }) 
      })
    }
    else if (this.props.match.params.id === 'new' ) { 
      // crear nuevo schema vacío, 
      this.setState({ bolo: new ActuacionSchema(), save: insertOne });
    }     
  }

  render() {
    let css = { padding: 20 }
    let cssLink = { color: 'grey', textDecoration: 'none'}
    let linkToCalendario = <Link to={`/calendario`} style={cssLink} > ver calendario </Link>
    let linkToHome = <Link to={`/promhome`} style={cssLink} > Home </Link>

    return (
      <div style={css}>        
        {/* resumen del estado de la actuación */}
        {linkToCalendario} {nbsp(5)} {linkToHome}
        {br(1)}
        <span> {this._rResumenActividad()} </span>
        
        
        {/* primera línea del formulario, fecha, etc. */}
        <div hidden={this.state.hideInputsBolo} className="form-inline">
          {this._rBoloNombre()}
          {nbsp(10)}
          {this._rBoloFechaPrevista()}
          {nbsp(10)}
          {this._rSelectSala()}
          {nbsp(10)}
          {this._rSelectTarifa()}
        </div>

        {br(2)}
        {/* gestión contabilidad */}
        <div> {this._rContabilidad()} </div>
        
        
        
        {br(10)}
        <pre> {JSON.stringify(this.state.bolo, null, 2)} </pre>
      </div>
    )
  }

  // datos mínimos controlan el botón de guardar:
  datosMinimos(){
    return this.state.bolo.fechaPrevista &&
    this.state.bolo.producto.nombre &&
    this.state.bolo.sala.nombre && this.state.bolo.sala.ciudad 
  }

  
  _rContabilidad(){
    let costes = 0
    let beneficios = 0
    return (
      <div>
        {/* títulos */}
        {/* sirve de click para el toggle de inputs del newApunte */}
        <div 
          onClick={ e => {
            this.setState({ hideInputsNewApunte: !this.state.hideInputsNewApunte})
          }}
          title="click añade apunte"
          className="row">
          <div className="col"> Concepto </div>
          <div className="col text-right"> Coste </div>
          <div className="col text-right"> Beneficio </div>
          <div className="col text-right"> Saldo </div>
        </div>   

        {/* inputs para nuevo apunte contable */}
        {/* está oculto inicialmente */}
        <div 
          hidden={this.state.hideInputsNewApunte}
          className="row">
          <div className="col">
            <input 
              onChange={ e => { this.setState({ newApunte: {...this.state.newApunte, concepto: e.target.value} }) }}
              placeholder='concepto'
              className="form-control"/>
          </div>
          <div className="col text-right">
            <input 
              onChange={ e => { this.setState({ newApunte: {...this.state.newApunte, coste: parseFloat(e.target.value) } }) }}
              type="number"
              placeholder='coste'
              className="form-control"/>
          </div>
          <div className="col text-right">
            <input 
              onChange={ e => { this.setState({ newApunte: {...this.state.newApunte, beneficio: parseFloat(e.target.value) } }) }}
              type="number"
              placeholder='beneficio'
              className="form-control"/>

          </div>
          <div className="col text-right">
            <span 
              hidden={(!this.state.newApunte.concepto) }
              onClick={ e => {
              // aquí se añade a contabilidad el newApunte, se vacía y se ocultan los inputs
              let newContabilidad = this.state.bolo.contabilidad
              newContabilidad.push(this.state.newApunte)
              this.setState({ 
                bolo: {...this.state.bolo, contabilidad: newContabilidad },
                newApunte: new ApunteSchema({concepto: '', coste: 0, beneficio: 0}),
                hideInputsNewApunte: true
              })

            }}
            className="cpointer text-success"> añadir </span>
          </div>
        </div>   

        <hr />

        {/* el map() para enseñar la contabilida del acto */}
       {this.state.bolo.contabilidad.map( (apunte, index) => {
         costes += apunte.coste
         beneficios += apunte.beneficio
         let saldo = apunte.beneficio - apunte.coste
         return <div key={index}>
         <div  className="row" style={ index % 2 === 0 ? {} : {backgroundColor: 'rgb(232, 238, 232)'} }>
           <div 
            onDoubleClick={ e => {
              let newContabilidad = this.state.bolo.contabilidad
              newContabilidad.splice(index, 1)
              this.setState({ bolo: {...this.state.bolo, contabilidad: newContabilidad } })
            }}
            title="doble click borra"
            className="col"> {apunte.concepto} </div>
           <div className="col text-right"> {apunte.coste.toFixed(2)} </div>
           <div className="col text-right"> {apunte.beneficio.toFixed(2)} </div>
           <div className="col text-right"> {saldo.toFixed(2)} </div>
         </div>

        </div>
       })}

       {/* resúmenes fuera del map */}
      {br(2)}
        <div className="row" style={{ border: '1px dotted' }}>
          <div className="col"> totales </div>
          <div className="col text-danger text-right"> {costes.toFixed(2)} </div>
          <div className="col text-right" style={{ color: 'green' }}> {beneficios.toFixed(2)} </div>
          <div className="col text-primary text-right"> {(beneficios - costes).toFixed(2)} </div>
        </div>

      </div>
    )
  }

  _rResumenActividad(){
    let css={ padding: 20, border: '1px solid' }
    return (
      <div style={css} className="bolo">
        <span 
          onClick={ this.aMongo }
          hidden={!this.datosMinimos()} style={{ color: 'red'}}>
          guardar cambios
        </span>
        <h4 
          onClick={ e => this.setState({ hideInputsBolo: !this.state.hideInputsBolo})}
          className="cpointer"> 
          {this.state.bolo.producto.nombre || 'nombre del espectáculo'} 
        </h4>
        fecha: {this.state.bolo.fechaPrevista} {nbsp(10)}
        sala: <Link to={`/sala/${this.state.bolo.sala._id}`}> {this.state.bolo.sala.nombre}  </Link> 
        {nbsp(1)} ({this.state.bolo.sala.ciudad})
        <Link to={`/tarifas/${this.state.bolo.sala._id}`} hidden={!this.state.bolo.sala._id}> <small> gestionar tarifas </small> </Link>        
      </div>
    )
  }

  _rSelectTarifa(){
    return (
      <div>        
        <select id="tarifa" className="form-control">
          <option> tarifas </option>
          {this.state.bolo.sala.tarifas.map( (t,i) => <option key={i} value={i}> {t.nombre} </option> )}
        </select>
        {nbsp(5)}
        <span onClick={ e => {
          let tarifa = document.getElementById('tarifa').value
          let origen = this.state.bolo.sala.tarifas[tarifa]
          let newContabilidad = this.state.bolo.contabilidad
          let apunte = new ApunteSchema({ 
            concepto: `${this.state.bolo.sala.nombre} alquiler ${origen.nombre}`,
            beneficio: origen.beneficio,
            coste: origen.coste
          })
          newContabilidad.unshift(apunte)          
          this.setState({ bolo: {...this.state.bolo, contabilidad: newContabilidad } })
          console.log(newContabilidad)
        }}> aplicar tarifa </span>
      </div>
      
    )
  }

  _rBoloNombre(){
    // TODO: esto tiene que estar también en un selector no a mano
    // porque tiene muchos más datos asociados al espectáculo
    return (
      <input 
        value={this.state.bolo.producto.nombre}
        onChange={e => this.setState({ 
          bolo: {...this.state.bolo, producto: {...this.state.bolo.producto, nombre: e.target.value } } 
        })}
        placeholder="espectáculo"
        size="30"
        className="form-control"/>
    )
  }

  _rBoloFechaPrevista(){
    return (
      <input 
        value={this.state.bolo.fechaPrevista}
        onChange={e => this.setState({ bolo: {...this.state.bolo, fechaPrevista: e.target.value} }) }
        type="date"
        className="form-control"
      />
    )
  }
  
  _rSelectSala(){
    return (
      <select         
        onChange={e => {
          // el <option> guarda el index del array, con éste se extrae el elemento del array salas y se guarda
          // en el bolo actual
          let sala = this.state.salas[e.target.value];
          this.setState({ bolo: {...this.state.bolo, sala: sala} })
        }}
        className="form-control">
        <option> salas </option>
        {this.state.salas.map( (sala, i) =>  <option key={sala._id.toString()} value={i}> {sala.nombre} </option>  )}

      </select>
    )
  }
}


// API:
// crear sala nueva: <Link to={`/sala/0`} > crear Sala </Link>
// editar sala: <Link to={`/sala/${_id}`} > gestionar Sala </Link>
export class GestionSala extends Component {
  // TODO 
  // 
  state = {
    sala: {},
    save: null // aquí se guarda el enlace a la función de guardar insertOne() o updateOne();
  }

  componentDidMount(){
    // si el parámetro de url tiene 24 chars, parece un _id de Mongo else será nuevo:
    // la clase SalaModel no tiene _id revisar
    if (this.props.match.params.id.length === 24) { 
      // recuperar la sala de Stitch mediante findSort()
      let api = { db: db, collection: collection, find: {_id: new ObjectID(this.props.match.params.id) }, sort: {} }
      findSort(api).then( array => 
        this.setState({ sala: array[0], save: updateOne }) 
      )
    }
    else if (this.props.match.params.id === 'nueva' ) { 
      // crear nuevo schema vacío, 
      this.setState({ sala: new SalaSchema(), save: insertOne });
    }     
  }

  // Database Management:

  aMongo = e => {
    let api = {db: db, collection: collection, doc: this.state.sala }  
    this.state.save(api).then( resp => console.log(resp))    
  }

  // Events:
  changeInputText = e => {
    this.setState({ 
      sala: {...this.state.sala, [e.target.name]: e.target.value }
    })
  }

  // renders:
  renderInputNombre = () => {
    return <input 
      name="nombre"
      value={this.state.sala.nombre}
      onChange={ this.changeInputText }
      className="form-control"
      placeholder="nombre de la sala"
      size="50"
    />
  }

  renderInputCiudad = () => {
    // TODO: la ciudad no debería escribirse a mano!!
    return <input 
      name="ciudad"
      value={this.state.sala.ciudad}
      onChange={ this.changeInputText }
      className="form-control"
      placeholder="ciudad"
    />
  }

  renderInputPais = () => {
    return <input 
      name="pais"
      value={this.state.sala.pais}
      onChange={ this.changeInputText }
      className="form-control"
      placeholder="país"      
    />
  }

  render() {
    let cssLink = {color: 'grey', textDecoration: 'none'}
    let linkToSalas = <Link to={`/salas/`} className="cpointer" title="ir a salas" style={cssLink}> Salas </Link>
    let linkToTarifas = <Link to={`/tarifas/${this.state.sala._id}`} style={cssLink}> gestionar tarifas </Link>
    let linkToHome = <Link to={`/promhome`} style={cssLink} > Home </Link>

    return (
      <div style={{padding: 20}}>
        {linkToTarifas} {nbsp(3)} {linkToSalas} {nbsp(3)} {linkToHome}
        {br(2)}
        {/* nombre Sala, ciudad y a ver qué cabe en una línea */}
        <div className="form-inline">
          {this.renderInputNombre()}
          {this.renderInputCiudad()}
          {this.renderInputPais()}
          {/* esta pieza graba cambios */}
          {nbsp(10)}
          <span onClick={ this.aMongo }> grabar </span>          
        </div>
       

        {br(3)}
        
        <pre style={ {backgroundColor: 'lightgrey'} }> {JSON.stringify(this.state.sala, undefined, 2)} </pre>
      </div>
    )
  }
}

export class TarifasSala extends Component {
  state = {
    sala: { tarifas: [] },
    newTarifa: { nombre: '', coste: 0, beneficio: 0 },
    cambios: 0
  }

  componentDidMount(){
    // traer la sala de mongo
    findSort({db: 'promusic', collection: 'salas', find: {_id: new ObjectID(this.props.match.params.id) }, sort: {} })
    .then( resp => { this.setState({ sala: resp[0] }) })
  }

  _rFormTarifa = () => {
    return (
    <div className="form-inline">
      {/* input para el nombre de la tarifa */}
      <input 
        onChange={ e => {
          this.setState({ newTarifa: {...this.state.newTarifa, nombre: e.target.value } })
        }}
        value={this.state.newTarifa.nombre}
        className="form-control"
        size="40"
        placeholder="nueva tarifa" />    
      {nbsp(5)}
      {/* input para el coste */}
      <input 
        onChange={e => {
          this.setState({ newTarifa: { ...this.state.newTarifa, coste: parseFloat(e.target.value) } })
        }}
        value={this.state.newTarifa.coste}
        title = 'coste neto'
        style = { { color: 'red' } }
        className="form-control"
        type="number"/> {nbsp(2)} €

      {nbsp(5)}
      {/* input para el beneficio */}
      <input 
        onChange={ e => {
        this.setState({ newTarifa: {...this.state.newTarifa, beneficio: parseFloat(e.target.value) } })
      }}
        value={this.state.newTarifa.beneficio}
        title = 'beneficio neto'
        className="form-control"
        style = { { color: 'green' } }
        type="number" /> {nbsp(2)} €

      {nbsp(10)}
      <span onClick={ this.anadirTarifa }> añadir </span>
    </div>  
    )
  }

  aMongo = () => {
    updateOne({ db: 'promusic', collection: 'salas', doc: this.state.sala })
    .then( resp => { resp.modifiedCount === 1 ? this.setState({ cambios: 0}) : console.log('') })

  }

  anadirTarifa = () => {
    // añade temporalmente en RAM una tarifa al array tarifas de la sala y UX
    // añadir la tarifa actual a la sala y al estado
    let newTarifas = this.state.sala.tarifas
    let newTarifa = this.state.newTarifa
    newTarifa._id = new ObjectID()
    newTarifas.push(this.state.newTarifa)        
    this.setState({ sala: {...this.state.sala, tarifas: newTarifas }, newTarifa: {}, cambios: this.state.cambios + 1 })
    // Limpiar inputs
    document.querySelectorAll('input').forEach( e => e.value = '')
  }
  
  render() {
    let css = { padding: 10, backgroundColor: ''}
    let linkToSalas = <Link to={`/salas/`} className="cpointer" title="ir a salas" style={{ color: 'grey', textDecoration: 'none'}}> Salas </Link>
    return (      
      <div style={css}>

        {/* resumen de la sala para ver dónde estamos */}
        <Link to={`/sala/${this.props.match.params.id}`} className="cpointer h4" title="volver"> 
          {this.state.sala.nombre} 
        </Link>

        {nbsp(3)} {this.state.sala.ciudad}

        {/* sitio para ver si se ha cambiado algo y si graba mongo */}
        {nbsp(10)} <small hidden={this.state.cambios === 0}>  (cambios sin guardar {this.state.cambios}) </small>
        
        {/* el botón para grabar en mongo */}
        {br(2)} <span onClick={ this.aMongo } className="cpointer" title="subir a Mongo"> guadar </span>

        {/* enlace a salas */}
        {nbsp(5)} {linkToSalas}

        <hr />

        {/* formulario para añadir nueva tarifa */}
        {this._rFormTarifa()}

        {/* tarifas actuales de la sala */}
        {br(2)}
        <hr /> 
        {this._rCabeceraTarifas()}
        {this.state.sala.tarifas.map( this._rTablaTarifas ) }

        {/* <pre> {JSON.stringify(this.state, null, 2)} </pre> */}

      </div>
    )
  }

  _rCabeceraTarifas(){
    let css = { color: 'black'}
    return <div className="row padding" style={css}>
      <div className="col-2">nombre de tarifa</div>
      <div className="col-1 text-right">coste</div>
      <div className="col-1 text-right">beneficio</div>
    </div>
  }

  _rTablaTarifas = (t, i) => {
    return (
      <div className="row" style={{ paddingLeft: 5 }}>
        {/* título, doble click borra */}
        <div 
          onDoubleClick={ e => {
            let newTarifas = this.state.sala.tarifas
            newTarifas.splice(i, 1)
            this.setState({ sala: {...this.state.sala, tarifas: newTarifas }, cambios: this.state.cambios + 1 })
          }}
          key={i}  
          title="doble click borra tarifa"        
          className="col-2 cpointer">           
          {t.nombre}         
        </div>
        {/* costes */}
        <div className="col-1 text-right"> {t.coste} </div>
        <div className="col-1 text-right"> {t.beneficio} </div>
      </div>
    )
  }
}