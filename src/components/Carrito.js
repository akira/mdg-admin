import React from 'react';
import '../App.css';

var productos = [
  { _id: '1', nombre: 'Oso Panda'},
  { _id: '2', nombre: 'Gato'},
  { _id: '3', nombre: 'Oso Gris'}
]

class ShowProducto extends React.Component {
  state = { comprado: false }


  render() {
    let style = { padding: 20, float: 'left'}
    return (
      <div style={style}>
        {this.props.p._id}, {this.props.p.nombre} <br />

        <span 
          hidden={this.state.comprado}
          onClick={ e => { 
            this.setState({ comprado: true }); 
            this.props.addToCar(this.props.p)
          }}> comprar </span>

        <span
          hidden={!this.state.comprado}
          onClick={e => {
            this.setState({ comprado: false });            
            this.props.delFromCart(this.props.p._id)
          }}> quitar </span>          

      </div>
    )
  }
}


export class Test extends React.Component {
  state = { carrito: [] }

  addToCar = producto => {
    let newCart = this.state.carrito;
    newCart.push(producto);
    this.setState({ carrito: newCart })
  }

  delFromCart = _id => {
    let newCart = this.state.carrito;
    let index = newCart.findIndex( p => { return p._id === _id})
    newCart.splice(index, 1);
    this.setState({ carrito: newCart })
  }

  viewProduct = (p, i) => {
    let style = {padding: 20}
    return <div style={style}>
      {p.nombre}
    </div>
  }

  render(){
    let style = {padding: 20}
    // let style2 = { padding: 20, float: 'right'}
    return (<div style={style}>
      
      {productos.map( (p, i) => <ShowProducto key={i} p={p} addToCar={this.addToCar} delFromCart={this.delFromCart} /> )}
      <br />
      tu compra <br />

      <div> 
        {this.state.carrito.map( this.viewProduct )} 
      </div>
      
      {/* {JSON.stringify(this.state, undefined, 2)} */}
    </div>)
  }
}
