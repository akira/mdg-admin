import React, { Component } from 'react';
import { UserPasswordCredential, GoogleRedirectCredential } from "mongodb-stitch-browser-sdk";
import { stitch } from '../index';


export default class Login extends Component {
  constructor(){
    super()
    this.state = { 
      username: '', 
      password: '',
      user: stitch.auth.user ? stitch.auth.user : null
    }
  }

  login = () => {
    stitch.auth.loginWithCredential(new UserPasswordCredential( this.state.username, this.state.password))
    .then( () =>{ this.setState({ user: stitch.auth.user }); window.location.href = '/' }) 
  }

  loginWithGmail = () => {
    stitch.auth.loginWithRedirect(new GoogleRedirectCredential('http://localhost:8000/') )          
  }

  logout = () => {
    stitch.auth.logout().then( n => window.location.href = '/' )
  }

  render() {
    return (
      <div className="container">     
        <br />
        <div className="form-inline">
          <input 
              onChange={ e => this.setState({username: e.target.value}) }
              className="form-control"
              placeholder="nombre de usuario" />
          <input 
            type="password" 
            onChange={ e => this.setState({password: e.target.value}) }
            className="form-control"
            placeholder="password" />
          <button onClick={this.login} className="btn btn-primary"> Stitch Login </button>
          <button onClick={this.loginWithGmail} className="btn btn-primary"> Gmail Login </button>
          <button onClick={this.logout} className="btn btn-primary"> Logout </button>

        </div>
      </div>
    )
  }
}
