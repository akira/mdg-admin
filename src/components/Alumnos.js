import React, { Component } from 'react'
import { br, nbsp, findSort } from '../index';
import { Link } from 'react-router-dom'

const iconNuevo = (<i className="material-icons" title="añadir"> add_circle_outline </i> );
const links = { textDecoration: 'none', color: 'grey'}

export class Alumnos extends Component {
  constructor(props){
    super(props);
    this.state = { 
      alumnos: [],
      debug: false,
      stats: { sinGit: 0 }      
    }
  }

  componentDidMount(){ this.getAlumnos() }

   getAlumnos = () => {
    findSort({ db: 'domingo', collection: 'alumnos', find: {}, sort: { nombre: 1 } })
    .then( data =>{ this.setState({ alumnos: data }); console.log(data); this.setStats() });
  }

  setStats = n => {
    let sinGit = this.state.alumnos.filter( a => { return a.git === '' || a.git === undefined })
    let sinEmail = this.state.alumnos.filter( a => { return a.email === '' || a.email === undefined })
    let sinDevTargets = this.state.alumnos.filter( a => { return a.devTargets  === undefined || a.devTargets.length === 0  })    
    console.log('sinDevTargets', sinDevTargets)
    this.setState({ stats: {sinGit: sinGit.length, sinEmail: sinEmail.length, sinDevTargets: sinDevTargets.length } })
  }
 
  showAlumno = (a) => {
    let sinGit = a.git === '' || a.git === undefined ;
    let sinEmail = a.email === '' || a.email === undefined;
    let sinTargets = a.devTargets === undefined || a.devTargets.length === 0;    
    return (
      <div key={a._id} style={{padding: 5}}>      
        <Link to={'/alumno/' + a._id} className="cpointer nombre-alumno-lista"> {a.nombre} </Link>
        {nbsp(2)}
        <small hidden={!sinTargets} className="text-danger"> targets! </small>        
        <small hidden={!sinGit} className="text-danger"> Git! </small>         
        <small hidden={!sinEmail} className="text-danger"> email! </small>
        <pre hidden={!this.state.debug}> { JSON.stringify(a, undefined, 2) } </pre>
      </div>
    )
  }

  render() {
    let fs = { fontSize: '15px' }
    return (
      <div className="container" style={fs}>     
        Debugging:{nbsp(2)} <input type="checkbox" onChange={ e => this.setState({ debug: !this.state.debug }) } />
        {nbsp(2)} <span style={fs}> {this.state.alumnos.length} activos </span> 
        {nbsp(2)} <span style={fs}> {this.state.stats.sinGit} sin Git </span> 
        {nbsp(2)} <span style={fs}> {this.state.stats.sinEmail} sin email </span> 
        {nbsp(2)} <span style={fs}> {this.state.stats.sinDevTargets} sin targets </span> 

        {nbsp(2)} <span style={fs}> <Link to="/alumno/nuevo" style={links}> {iconNuevo} </Link>  </span> 
        
        {br(2)}
        { this.state.alumnos.map( this.showAlumno ) }

      </div>
    )
  }
}
