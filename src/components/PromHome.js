import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {nbsp} from '../index'
export class PromHome extends Component {
  render() {
    let cssLink = { color: 'grey', textDecoration: 'none'}
    let linkCalendar = <Link to={`/calendario/`} style={cssLink}> calendario </Link>
    let linkSalas = <Link to={`/salas/`} style={cssLink}> salas </Link>
    let linkNewBolo = <Link to={`/actividad/new`} style={cssLink}> programar bolo </Link>
    return (
      <div style={{padding: 20}}>
        {linkCalendar} {nbsp(3)}
        {linkSalas} {nbsp(3)}
        {linkNewBolo} {nbsp(3)}
      </div>
    )
  } 
  
}
