import React, { Component } from 'react';
import { mongo, stitch, nbsp, br } from '../index'
import {ObjectID} from 'bson';



export class EsquemaGrupo{  

  constructor() {
    this.nombre = ''; // ok
    this.componentes = [];
    this.autor = stitch.auth.user.id;
  }

  insertOneToDb() {
   return mongo.db('proMusic').collection('grupos').insertOne(this)
  }
  findOneAndReplaceToDb(id){

   return mongo.db('proMusic').collection('grupos').findOneAndReplace({_id: new ObjectID( id )}, this)
  }
  deleteOneToDb(id){
   return mongo.db('proMusic').collection('grupos').deleteOne({_id: new ObjectID(id)})
  }

  static listadoGrupo(sort){
    // USO: EsquemaGrupo.listadoGrupo({ nombre: -1 }).then( l => { console.clear(); console.log(l) })

    let s = sort ? sort : { nombre: 1 }
    return mongo.db('proMusic').collection('grupos').aggregate([
      {$sort: s }
    ]).toArray()
  }

  static buscarCompetencias(competencia, sort) {
  // USO: EsquemaGrupo.buscarCompetencias('guitarra', {"componentes.competencias.competencia": 1} ).then( d => { console.clear(); console.log(d) });

    let s = sort ? sort : { "componentes.nombre": 1 }
    return mongo.db('proMusic').collection('grupos').aggregate([
      { $unwind: "$componentes"},
      { $unwind: "$componentes.competencias"},
      { $match: { "componentes.competencias.competencia": { $regex: competencia, $options: 'i'}  }},
      { $project: { "componentes.nombre": 1, "componentes.competencias": 1 } },
      { $sort: s }     

    ])
    .toArray()
  }

  static competenciasArtista(artista) {
    // USO: EsquemaGrupo.competenciasArtista('villa').then( d => { console.clear(); console.log(d) });

    return mongo.db('proMusic').collection('grupos').aggregate([
      { $unwind: "$componentes"},
      { $unwind: "$componentes.competencias"},
      { $match: { $or: [
        { "componentes.nombre": { $regex: artista, $options: 'i'}  },
        { "componentes.apellido": { $regex: artista, $options: 'i'}  }
      ] } },
      { $project: { nombre: 1,  "componentes.nombre": 1, "componentes.apellido": 1, "componentes.competencias": 1, _id: 0 } }
    ])
    .toArray()    
  }

  static cacheGrupos(sort){  
    // USO: EsquemaGrupo.cacheGrupos({ _id: 1 }).then( d => { console.clear(); console.log(d) });
    let s = sort ? sort : { cache: -1 }
    return mongo.db('proMusic').collection('grupos').aggregate([
      { $unwind: "$componentes"},
      { $unwind: "$componentes.competencias"},
      { $group: { 
        _id: "$nombre", 
        cache: { $sum: "$componentes.competencias.cache" } 
      } },
      { $sort: s }      
    ])
    .toArray()
  }

  static cacheArtistas(sort){  
    // USO: EsquemaGrupo.cacheArtistas({ _id: 1 }).then( d => { console.clear(); console.log(d) });

    let s = sort ? sort : { cache: -1 }
    return mongo.db('proMusic').collection('grupos').aggregate([
      { $unwind: "$componentes"},
      { $unwind: "$componentes.competencias"},
      { $group: { 
        _id: { nombre: "$componentes.nombre", apellidos: "$componentes.apellido"}, 
        competencias: { $sum: 1 }, 
        cache: { $avg: "$componentes.competencias.cache" } 
      }},
      { $sort: s }      
    ])
    .toArray()
  }
  
  static  buscarGrupoPorNombre(nombre, sort) {
    let s = sort ? sort : {nombre: 1}
    return mongo.db('proMusic').collection('grupos').aggregate([
      { $match: { nombre:{ $regex: new RegExp(nombre), $options:'i'}}},
      { $sort: s }
    ]).toArray()
  }

  static listadoArtistas(nombre){

    return mongo.db('proMusic').collection('grupos').aggregate([      
      { $unwind: "$componentes"}, 
      { $sortByCount: "$componentes.nombre" },
      { $match: { _id: { $regex: nombre ? nombre : '', $options:'i'}}},
    ]).toArray()
  }

  static listadoCompetencias(competencia){

    return mongo.db('proMusic').collection('grupos').aggregate([
      { $unwind: "$componentes"}, 
      { $unwind: "$componentes.competencias" },
      { $sortByCount: "$componentes.competencias.competencia" },
      { $match: { _id: { $regex: competencia ? competencia : '', $options:'i'}}},

    ]).toArray()
  }
}

export class EsquemaArtistas {
  constructor(){
    this.nombre = ''; //ok
    this.apellido = ''; // ok
    this.competencias = []; //ok
    this.telefono = '';//ok
    this.comentario = '';//ok
    this.autor = stitch.auth.user.id;
  }
}


export class CompetenciasCache {
  constructor(){
    this.competencia = '';
    this.cache = 0;
  }
}



export default class Grupo extends Component {

  componentDidMount(){
    console.clear();
    EsquemaGrupo.listadoArtistas().then( d => {  console.log( 'artistas:', d) });
    EsquemaGrupo.listadoCompetencias('guitarra').then( d => { console.log( 'competencias:',d) });

    // EsquemaGrupo.listadoGrupo({ nombre: -1 }).then( l => { console.clear(); console.log(l) })


    if (this.props.match.params.id === 'nuevo') {      
      this.setState({
       grupo: new EsquemaGrupo()
      })
    }
    else { this.getOne() }
  }

  getOne = n =>{

    let idBueno = new ObjectID(this.props.match.params.id)
    this.mongoGrupo.findOne({_id: idBueno})
    .then( g => {console.log(g); this.setState({grupo: Object.assign(new EsquemaGrupo(), g)})})
    .catch(e => {console.log(e)})
  }

  renderInputuNombreGrupo = () => {
    return(
      <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
        <input className="form-control"
          placeholder="Nombre Grupo" 
          size="35"
          value={this.state.grupo.nombre}
          onChange={ e => {this.setState({
            grupo: Object.assign( new EsquemaGrupo(),{...this.state.grupo, nombre: e.target.value})
          })}
        }/>
      </div>
    )
  
  }
  renderInpunArtista = () => {
    return(
      <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
  
         {/* <label className="h4">Artista</label> */}
         <input className="form-control"
         placeholder="Nombre"
         size="20"
         value={this.state.artista.nombre}
         onChange={ e =>{ this.setState({
           artista: Object.assign( new EsquemaArtistas(), {...this.state.artista, nombre: e.target.value})
         })}} /> {nbsp(1)}
  
         {/* <label className="h2">Apellido</label> */}
         <input className="form-control"
         placeholder="Apellido"
         size="20"
         value={this.state.artista.apellido}
         onChange={ e =>{ this.setState({
           artista: Object.assign( new EsquemaArtistas(), {...this.state.artista, apellido: e.target.value})
         })}} />{nbsp(1)}
  
        {/* <label className="h3">Teléfono</label> */}
         <input className="form-control"
         placeholder="Teléfono"
         value={this.state.artista.telefeno}
         onChange={ e =>{ this.setState({
           artista: Object.assign( new EsquemaArtistas(), {...this.state.artista, telefono: e.target.value})
         })}} />
     
      </div>
    )
  
  }
  renderInpunCompetenciasCache = () => {
    return(
      <div className="form-inline" style={{ margin: "10px 15px 10px 15px"}}>
  
        {/* <label className="h3">competencias</label> */}
         <input className="form-control"
         placeholder="competencia"
         size="20"
         value={this.state.nuevaCompetencia.competencia}
         onChange={ e =>{ this.setState({
          nuevaCompetencia: Object.assign( new CompetenciasCache(), {...this.state.nuevaCompetencia, competencia: e.target.value})
         })}} />{nbsp(1)}
  
        {/* <label className="h3">Cache</label> */}
          <input className="form-control" 
         placeholder="Cache"
         size="5"
         type = "number"
         value={this.state.nuevaCompetencia.cache}
         onChange={ e =>{ this.setState({
          nuevaCompetencia: Object.assign( new CompetenciasCache(), {...this.state.nuevaCompetencia, cache: parseFloat(e.target.value)})
         })}} />{nbsp(1)}
  
         {/* añade al array de competencias y cache */} 
        <i className="material-icons" title="añade"
       style={{color:"#753086"}}
       onClick={ e => {
        let copia = this.state.artista.competencias
        copia.push(this.state.nuevaCompetencia)
        this.setState({ artista: Object.assign(new EsquemaArtistas(), {...this.state.artista, competencias: copia })})
        console.log(this.state.artista)
     }}> check_circle </i> 
  
      </div>
    )
  } 
  renderInpunComentarioArtista = () => {
    return(
      <div style={{ margin: "10px 15px 10px 15px"}}>
        <label className="h3">Comentario</label>
        <textarea className="form-control"
          placeholder="comentarios"
          rows="5"
          value = {this.state.artista.comentario}
          onChange = { e => {this.setState({
            artista: Object.assign( new EsquemaArtistas(), {...this.state.artista, comentario: e.target.value})
          })}}
          />
        
  
      </div>
    )
  }
  renderIconoAnadirArtista = () => {
    return(
     <i className="material-icons" title="ok"
     style={{color:"#753086"}}
     onClick={ e => {
     let copia = this.state.grupo.componentes    
     copia.push({...this.state.artista, id: new ObjectID() })
     this.setState({ grupo: Object.assign(new EsquemaGrupo(), {...this.state.grupo, componentes: copia})})
     this.setState({ artista: new EsquemaArtistas()})
     console.log(this.state.grupo)
      }}>thumb_up </i> 
    )
  }
  renderIconoGrabar = () => {
    return(
      <i className="material-icons"
           style={{color: "#753086"}}  
           title="grabar"
          //  hidden={this.props.match.params.id}
             onClick={() => {
              this.state.grupo.insertOneToDb()
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }}>save</i>
    )
  }  
  renderIconoEditar   = () => {
    return(
      <i className="material-icons"
             style={{color:"#753086"}} 
            title="editar"
            hidden={!this.props.match.params.id}
            onClick={ () => { this.state.grupo.findOneAndReplaceToDb(this.props.match.params.id )
              .then( d => {console.log( d )})
              .catch( e => {console.log( e )})} 
            } >create</i>
    )
  }
  renderIconoBorrar = () => {
    return(
      <i className="material-icons" 
            title="Borar"
            style={{color:"#8B0000"}}
            hidden={!this.props.match.params.id} 
            onClick={ e =>{this.state.grupo.deleteOneToDb(this.props.match.params.id)
              .then( d => {console.log( d )})
              .catch(e => {console.log( e )})
            }} >
          delete_forever
        </i>
    )
  }

  mongoGrupo = mongo.db('proMusic').collection('grupos')

  state = { 
    grupo: new EsquemaGrupo(), 
    artista: new EsquemaArtistas(),
    nuevaCompetencia: new CompetenciasCache()
   }

  render() {
    return (
      <div>
        <div className = "text-center">
            <span className="h2"> {this.state.grupo.nombre} </span>{br(1)}
            {/*grabar la espectaculo  */}
            {this.renderIconoGrabar()}{nbsp(2)}
            {/*edita la espectaculo  */}
            {this.renderIconoEditar()} {nbsp(2)}
            {/*Borra la espectaculo  */}
            {this.renderIconoBorrar()}
            {this.renderInputuNombreGrupo()}
          </div><hr/>

          <div className = "text-center">
            <span className="h4"> {this.state.artista.nombre} </span> 
            <div className = "form-inline">
            {this.renderInpunArtista()}
            {this.renderIconoAnadirArtista()}
            
            </div>
            {this.renderInpunComentarioArtista()}
            {this.renderInpunCompetenciasCache()}
              
          </div><hr/>


       <pre> { JSON.stringify(this.state, undefined,2)}</pre> 
      </div>
    )
  }
}
