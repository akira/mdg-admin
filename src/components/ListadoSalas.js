import React, { Component } from 'react';
import {mongo, br, nbsp } from '../index';
import { Link } from 'react-router-dom'


export class ListadoSalas extends Component {
  state = {
    salas: []
  }
  componentDidMount(){
    mongo.db('promusic').collection('salas').find({}).toArray()
    .then( salas => this.setState({ salas: salas}) )
  }
  render() {
    let css={ padding: 30}
    let cssLink = { color: 'grey', textDecoration: 'none'}
    let linkToHome = <Link to={`/promhome`} style={cssLink} > Home </Link>

    return (
      <div style={css}>
        <h4> salas </h4>
        {linkToHome}
        <hr />
        {this.state.salas.map( (sala, index) => <ResumenSala key={sala._id.toString()} sala={sala} />  )}
      </div>
    )
  }
}

class ResumenSala extends Component {
  render() {
    let css = {paddingLeft: 20 }
    let linkCss = {color: 'grey', textDecoration: 'none'}

    return (
      <div style={css} title="gestionar sala">
        <Link to={`/sala/${this.props.sala._id}`} 
          title="gestionar sala"
          style={linkCss}> 
          {this.props.sala.nombre} ({this.props.sala.ciudad}) 
        </Link>        
        {nbsp(3)}
        <Link to={`/tarifas/${this.props.sala._id}`} 
          title="gestionar tarifas"
          style={linkCss}> 
          ver tarifas
        </Link>                
        {br(1)}
      </div>
    )
  }
}