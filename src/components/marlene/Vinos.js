import React, { Component } from 'react'
import { mongo, br, nbsp } from '../../index'

const [db, collection] = ['marlene', 'vinos']

export default class Vinos extends Component {
  state = {
    totalDB: 0,
    paises: [],     // {_id: 'Spain', total: 6645}, ...
    vinos: [],
    country: '',    // 'Spain'
    variety: '',     // 'Albariño'
    variedades: []  // {_id: 'Albariño', total: 340}, ...
  }
  vinos = mongo.db(db).collection(collection)
  // constructor(){
  //   super()
  //   this.vinos = mongo.db(db).collection(collection)
  // }

  componentDidMount(){    
    this.vinos.count().then( total => this.setState({totalDB: total}))
    this.groupPaises();    
  }

  groupPaises = () => {
    // let vinos = mongo.db(db).collection(collection)
    let query = [
      { $group: { _id: '$country', total: { $sum: 1 } } },
      { $sort: { _id: 1 } }
    ]    
    this.vinos.aggregate(query).toArray().then( paises => {
      console.log(paises)
      this.setState({ paises: paises }) 
    })        
  }

  getVariedadesPais = () => {    
    let query = [
      { $match: {country: this.state.country }},
      { $group: { _id: '$variety', total: { $sum: 1 } } },
      { $sort: { _id: 1 } }
    ]
    // trae las variedades de ese país
    this.vinos.aggregate(query).toArray().then( variedades => {
      console.log(variedades);
      this.setState({ variedades: variedades })
    })
  }

  getVinosPais = () => {
    // trae los vinos del pais seleccionado en el renderSelectPaises
    mongo.db(db).collection(collection)
    .find({ country: this.state.country }).toArray()
    .then( v => this.setState({ vinos: v }))
  }

  testing = () => {
    let query = [ 
      { $match: { country: this.state.country, variety: this.state.variety } },
      { $project: { country: 1, variety: 1, title: 1 } }
    ]
    let vinos = mongo.db(db).collection(collection)
    vinos.aggregate(query).toArray().then( d => console.log(d))
  }

  render() {
    let css = { padding: 30}    
    let resumen = <div>
      País: { this.state.country }  {br(1)}
      variedades: {this.state.variedades.length } {br(1)}
      variedad: {this.state.variety }
    </div>

    return (
      <div style={css}>
        {/* <button onClick={ this.testing }> testing </button> */}
        países: {this.state.paises.length} {br(1)}
        vinos: {this.state.totalDB.toLocaleString()} {br(1)}
      
        <div className="form-inline">
          {this.renderSelectPaises()}          
        </div>


        <div className="form-inline" hidden={false}>          
          {this.renderSelectVariedad()} {nbsp(5)}
          <span onClick={e => document.getElementById('modalVino').style.display = 'block'}> mejor puntuación </span>
          <hr />

        </div>        

        {resumen}
        <hr />
        {/* {this.state.variedades.map( this.renderVariedad )} */}


        {this.renderModalVino()}
        {/* <pre> { JSON.stringify(this.state.vinos, undefined, 2) } </pre> */}
      </div>
    )
  }

  renderResumenVino = (vino, index) => {
    let css = {padding: 20}
    return (
      <div key={index} style={css}>
        <span style={{fontSize: 20}}> {vino.title} </span> {br(1)}
        designation: {vino.designation} {br(1)}
        bodega: <span> {vino.winery} </span> {br(1)}
        points: <span style={{fontSize: 15, color: 'green'}} title="puntuación"> {vino.points} </span> {nbsp(3)}
        price: <span style={{fontSize: 15, color: 'red'}} title="precio"> {vino.price} </span> {br(1)}
      </div>
    )
  }

  renderModalVino = () => {
    return (
      <div id="modalVino" className="modal2">
        
        <div className="modal2-content">
          <div 
            onClick={ () => document.getElementById('modalVino').style.display = 'none' }
            className="text-right"> 
            cerrar 
          </div>
          País: {this.state.country} {br(1)} 
          Variedad: {this.state.variety} {br(1)}           
          vinos: {this.state.vinos.length} {br(1)}          
          <hr />
          {this.state.vinos.map( this.renderResumenVino )}

        </div>
      </div>
    )
  }

  renderSelectPaises = () => {
    return (
      <select onChange={this._onChangePais} className="form-control">
        <option> Paises </option>
        {this.state.paises.map( (p, i) => 
          <option key={i} value={p._id}> 
            {p._id} &nbsp;&nbsp; (vinos: {p.total.toLocaleString()})  
          </option> 
        )}
      </select>
    )
  }


  renderSelectVariedad = () => {
    return (
      <select 
        // value={this.state.variety}
        onChange={this._onChangeSelectVariedad}
        className="form-control">     
        <option> variedades </option>
        {this.state.variedades.map( (variety, index) => (
          <option key={index} value={variety._id}> 
            {variety._id} (vinos: {variety.total})
          </option>) 
        )}
      
      </select>
    )
  }


  renderVariedad = (v, i) => {
    let cssVariedad = { fontSize: 20 }
    return (      
      <div key={i}> 
        <span 
          onClick={this._onClickVariedad}
          style={cssVariedad}> {v._id} </span> <small> {v.total} vinos </small> 
      </div>
    )
  }

  _onChangePais = e => {
    // guarda en state el pais del selector y se trae las variedades
    this.setState({ country: e.target.value }, () => { this.getVariedadesPais() })
  }

  _onChangeSelectVariedad = e => {
    this.setState({variety: e.target.value })
    let query = [
      { $match: {country: this.state.country, variety: e.target.value }},
      { $sort: { points: -1 } }
    ]
    this.vinos.aggregate(query).toArray().then( vinos => {
      this.setState({ vinos: vinos });
      document.getElementById('modalVino').style.display = 'block'
    })

  }

}
