import React, { Component } from 'react'
import  { mongo } from '../../index'


export class Stats extends Component {
  state = { vino: {} }
  vinos = mongo.db('').collection('')
  
  // aggregates posibles:
  vinosPorPais = [
    { $group: { _id: '$country', vinos: { $sum: 1 }} },
    { $sort: { vinos: -1 } }
  ]

  vinosPorPaisVariedad = [
    { $group: { _id: { country: '$country', variety: '$variety' }, vinos: { $sum: 1 }} },
    { $sort: { vinos: -1 } }
  ]    

  bodegasConMasPuntos = [
    { $group: { 
        _id: '$winery', 
        points: { $sum: '$points' }, 
        media: { $avg: '$points' },
        vinos: { $sum: 1 }
    }},    
    { $sort: { points: -1 } }
  ]

  bodegasConMasVinosYMejorMedia = [
    { $group: { 
        _id: { winery: '$winery', country: '$country' }, 
        points: { $sum: '$points' }, 
        media: { $avg: '$points' },
        vinos: { $sum: 1 }
    }},    
    { $sort: { vinos: -1, media: -1 } }
  ]

  bodegasMasCaras = [
    { $group: { _id: { winery: '$winery' }, price: { $avg: '$price' }} },    
    { $sort: { price: -1 } }
  ]

  vinosPorProvincia = [
    { $group: { _id: { province: '$province' }, vinos: { $sum: 1 }} },    
    { $sort: { vinos: -1 } }
  ]

  vinosPorRegionPais = [
    { $group: { _id: { region_1: '$region_1', country: '$country' }, vinos: { $sum: 1 }} },    
    { $sort: { vinos: -1 } }
  ]

  vinosPorVariedad = [
    { $group: { _id: { variety: '$variety' }, vinos: { $sum: 1 }} },    
    { $sort: { vinos: -1 } }
  ]

  catadoresConMasVinos = [
    { $group: { _id: '$taster_twitter_handle', vinos: { $sum: 1 }} },    
    { $sort: { vinos: -1 } }
  ]

  
  render() {
    return (
      <div>
      </div>
    )
  }
}
