import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import { findSort, br, nbsp, updateOne } from '../index';
import { ObjectId } from 'bson';
import './Targets.css'
import { BlogUtils } from './BlogUtils';


class TargetModel {
  constructor(){
    this.titulo = '';
    this.seccion = '';
    this.html = '';
    this.fechaInicio = '';
    this.fechaFin = '';
    this.gradoDificultad = 1;
    this.valoracion = 1;
  }
}


const dbname = 'domingo', collection = 'alumnos';
const iconOk = (<i 
  className="material-icons alert-success"   
  title="ok, guardar cambios"> check_circle_outline </i>)
const iconLapiz = (<i 
  className="material-icons " 
  style={{ fontSize: 15 }}
  title="modificar"> create </i>)
const iconSubirDificultad = (<i 
  className="material-icons" 
  style={{ fontSize: 15 }}
  title="subir dificultad"> thumb_up </i>)
const iconBajarDificultad = (<i 
  className="material-icons" 
  style={{ fontSize: 15 }}
  title="bajar dificultad"> thumb_down </i>)
const iconSubirValoracion = (<i
  className="material-icons"
  style={{ fontSize: 15 }}
  title="subir valoracion"> thumb_up </i>)
const iconBajarValoracion = (<i
  className="material-icons"
  style={{ fontSize: 15 }}
  title="bajar valoracion"> thumb_down </i>)  
const iconArchivar = (<i 
    className="material-icons" 
    style={{ fontSize: 15 }}
    title="cambiar archivado"> folder_open </i>)
const iconSaveDb = (<i
  className="material-icons"
  style={{ fontSize: 25 }}
  title="guardar en DB"> check_circle_outline </i>)
const iconNewTarget = (<i
    className="material-icons"
    style={{ fontSize: 25 }}
    title="nuevo target"> playlist_add </i>)  
const iconBack = (<i
  className="material-icons"
  style={{ fontSize: 25 }}
  title="volver al alumno"> replay </i>)  
const iconReorder = (<i
    className="material-icons"
    style={{ fontSize: 25 }}
    title="reordenar"> autorenew </i>)   
const iconEquis = (<i
  className="material-icons"
  style={{ fontSize: 25, color: 'black', backgroundColor: 'white' }}
  title="cerrar"> close </i>)       
const iconTools = (<i
  className="material-icons"
  style={{ fontSize: 25 }}
  title="abrir herramientas"> build </i>)     


export default class Targets extends Component {
  constructor(props){
    super(props);
    this.state = {  
      doc: { devTargets: [] }, // por si no tiene aún
      modifiedcount: 0,
      reorder: [], reorderField: 'titulo', reorderRev: false
    }
    window.onclick = (event) => {  
      if (event.target ===  document.querySelector('#modalReorder') ) { 
        document.querySelector('#modalReorder').style.display = 'none'
      }
    }            
  }

  componentDidMount(){
    this.getDoc();
  }

  getDoc = n => {
    findSort({ 
      db: dbname, 
      collection: collection,   
      find: { _id: new ObjectId(this.props.match.params.alumno) },
      sort: {}
    })
    .then( data => { 
      console.log(data); 
      this.setState({ doc: {...this.state.doc, ...data[0]}  } ) // data[0] porque findSort() devuelve siempre un array
    }); 
  }

  updateDoc = n => {
    let params = {db: dbname, collection: collection, doc: this.state.doc }
    updateOne(params).then( resp => this.setState({ modifiedcount: 0 }) )
  }

  
  newTarget = e => {
    let newTarget = new TargetModel();
    let newTargets = this.state.doc.devTargets;
    newTarget = {...newTarget, titulo: 'Nuevo Objetivo', seccion: 'sin sección'}
    newTargets.unshift(newTarget) // añadimos al principio del array
    // otra curiosidad en el cambio de state:
    // si no se hace así, crea el nuevo objetivo, pero como una copia del último de la lista !!!
    this.setState({
      doc: {...this.state.doc, devTargets: [] } 
    }, e => {
      this.setState({
        doc: {...this.state.doc, devTargets: newTargets },
        modifiedcount: this.state.modifiedcount + 1
      })
    });

  }

  // renders
  datosAlumno = e => {
    return (
      <div>
        
        <span> <b> { this.state.doc.nombre } </b></span> 
        {nbsp(10)}
        <span onClick={ this.updateDoc }> {iconSaveDb} </span> 
        {nbsp(10)}
        <span onClick={ this.newTarget }> <Link to={`/alumno/${this.state.doc._id}`} style={{color: 'grey'}}> {iconBack} </Link> </span>         
        {nbsp(10)}
        <span onClick={ this.newTarget }> {iconNewTarget} </span> 
        {this.formReorders()}
        {nbsp(10)}
        <span onClick={e => document.getElementById('modalTools').style = 'display: block'} > {iconTools} </span>
        <ModalTools />


        <br />

        <small> { this.state.doc.email } </small> <br /> 
        <small> { this.state.doc.git } </small> <br />

        <span 
          hidden={!this.state.doc.ediciones}> 
          <small>modificaciones: </small>
          {nbsp(3)}
          <span           
            className="text-danger"> 
            {this.state.doc.ediciones}  
          </span>          
        </span>

        {nbsp(10)}
        <span hidden={ this.state.modifiedcount === 0 }>
          <small> sin grabar en DB: {nbsp(3)}</small>
          
          <span           
            className="text-danger"> 
            {this.state.modifiedcount}  
          </span>
        </span>        

      </div>
    )
  }

  reorder = fieldName => {    
    let down = this.state.reorderRev ? 1 : -1 // para orden asc y desc
    let up = this.state.reorderRev ? -1 : 1 // para orden asc y desc
    let data = this.state.doc.devTargets || [];
    let reorder = data.sort( (a, b) => { return a[fieldName] < b[fieldName] ? down : up });
    this.setState({ reorder: reorder, reorderField: fieldName }, e => {
      document.querySelector('#modalReorder').style = 'display: block';
    })
  }

  modalReorder = e => {
    // se ve mucho código pero son un paar de span y el .map(); así se queda
    return (
    <div id="modalReorder" className="modal-reorder">
    {br(1)}
    <span
      onClick={e => document.querySelector('#modalReorder').style = 'display: none'}
      style={{ color: 'black', float: 'right' }}>
      {iconEquis}
      {nbsp(5)}
    </span>

      <div className="modal-reorder-content">   
        <p> ordenado por: {this.state.reorderField}  
        {nbsp(3)}
        <span
          onClick={ e => { 
            this.setState({ reorderRev: !this.state.reorderRev }, 
              this.reorder(this.state.reorderField)
            ) 
          }}
          title="este title no sale, sale el del icono"> 
          {iconReorder} 
        </span>
        </p>
        {this.state.reorder.map( (t,i) => (
            <div key={i} style={{padding: 5}}>            
              <span className="circle">{t.gradoDificultad}</span>
              {nbsp(3)}              
              {t.titulo} 
              {br(1)}
              <small className="circles">{t.fechaInicio}</small>
              {br(1)}  
              <small> {t.seccion} </small>
            </div>
            
          )   
        )}

        {/* <pre> { JSON.stringify(this.state.reorder, undefined, 2) } </pre> */}
      </div>
    </div>)
  }

  formReorders = e => {
    return (
    <div className="form-inline" style={{float: 'right'}}>
      <select 
        onChange={e => { this.reorder(e.target.value); }} 
        title="listar objetivos por este valor"
        className="form-control">
        {/* lo del defaultValue me protestaba por consola,  lo he probado y va;
            lo de los values así, era por otra cosa, debería ir con strings normales
        */}
        <option value={"titulo"} defaultValue> título </option> 
        <option value={"gradoDificultad"}> dificultad </option>
        <option value={"fechaInicio"}> fecha de inicio </option>
        <option value={"seccion"}> sección </option>
      </select>
      {nbsp(3)}
      <span onClick={ e => { this.reorder( this.state.reorderField ) } }> {iconReorder} </span>
    </div>
    )
  }
  render() {
    return (
      <div className="container">
        {this.datosAlumno()}
        {this.modalReorder()}
        <ListTargets targets={this.state.doc.devTargets} updateTarget={this.updateTarget} />        
      </div>
    )
  }

  // exportados:
  // actualizar 1 target:
  updateTarget = (newTarget, index) => {
    let newTargets = this.state.doc.devTargets || [] ; // por si no hay...
    newTargets[index] = { ...newTargets[index], ...newTarget }
    // let ediciones = this.state.doc.ediciones || 1;
    this.setState({
      doc: {...this.state.doc, devTargets: newTargets, ediciones: this.state.doc.ediciones + 1 || 1 },
      modifiedcount: this.state.modifiedcount + 1
    })
  }

} // end class Targets


export class ListTargets extends Component {  
  // componente supersencillo, recibe cosas y pasa cosas a ShowTarget
  render(){
    return (
      <div hidden={ this.props.targets.length === 0 }>     
      {this.props.targets.length} targets
      <hr />
        { this.props.targets.map( (t, i) => ( <ShowTarget key={i} target={t} index={i} {...this.props} /> ) )}        
      </div>
    )
  }
} // end class ListTargets

export class ShowTarget extends Component {
  state = {
    target: {...this.props.target},
    htmlOculto: true,
    tituloOculto: true,
    inputFechaOculto: true,
    inputSeccionOculto: true,
    archivadosOcultos: true
  }

  // app functions:

  // la principal que propaga al Componente padre los cambios importantes en los input
  updateTarget = n => {
    this.props.updateTarget(this.state.target, this.props.index);     
  }
  
  // gestión del grado de dificultad:
  subirDificultad = e => {
    this.setState({
      target: {...this.state.target, gradoDificultad: this.state.target.gradoDificultad + 1 }      
      }, this.updateTarget )
    // subir cambios al Componente Padre:
  }
  bajarDificultad = e => {
    this.setState({
      target: {...this.state.target, gradoDificultad: this.state.target.gradoDificultad - 1 }      
    }, this.updateTarget );
  }

// gestión del grado de valoracion:
  subirValoracion = e => {
    this.setState({
      target: {...this.state.target, valoracion: this.state.target.valoracion + 1 }      
      }, this.updateTarget )
    // subir cambios al Componente Padre:
    // this.props.subirDificultad(this.state.target.gradoDificultad, this.props.index);
  }
  bajarValoracion = e => {
    this.setState({
      target: {...this.state.target, valoracion: this.state.target.valoracion - 1 }      
    }, this.updateTarget );
  }  
  // ...

  // funciones render
  botonesDificultad = e => {  
    return (
      <div style={ { float: 'center' } }>
      {nbsp(3)}
      <span 
        onClick={e => this.setState({ tituloOculto: !this.state.tituloOculto })}
        className="cpointer"> {iconLapiz} </span> 
      {nbsp(3)}
      {/** grado de dificultad */}
        <small> dificultad {this.state.target.gradoDificultad} </small> 
        {nbsp(3)}        
        <span onClick={this.subirDificultad}> {iconSubirDificultad} </span>
        {nbsp(2)}
        <span onClick={this.bajarDificultad}> {iconBajarDificultad} </span>
      {/** grado de valoración */}
        {nbsp(15)}       
        <small> valoración {this.state.target.valoracion} </small> 
        {nbsp(2)}        
        <span onClick={this.subirValoracion}> {iconSubirValoracion} </span>
        <span onClick={this.bajarValoracion}> {iconBajarValoracion} </span>
      {/** archivar */}
      {nbsp(15)}    
      <span 
        onClick={e => 
          this.setState({ target: { ...this.state.target, archivado: !this.state.target.archivado } }, this.updateTarget) 
          }
      > {iconArchivar} </span>
      </div>
    )
  }

  bloqueFechaInicio = e => {
    // muestra la fecha y contiene el <div> ocultable para editarla
    return (
      <div>
         <small
          onClick={e => this.setState({ inputFechaOculto: !this.state.inputFechaOculto}) }
          title="click para cambiar"
          className="cpointer"> 
          {this.state.target.fechaInicio || 'sin fecha'} 
        </small> 

        <div hidden={this.state.inputFechaOculto} className="form-inline" style={{ float: 'right'}}>
          <input 
            value={this.state.target.fechaInicio}
            onChange={ e => this.setState({ 
              inputFechaOculto: true,
              target: {...this.state.target, fechaInicio: e.target.value }
            }, this.updateTarget) }
            type="date"
            className="form-control"
          />
        </div>
       
      </div>
    )
  }
  
  bloqueTitulo = e => {    
    return (<div>      
      <div
        onClick={ e => this.setState({ htmlOculto: !this.state.htmlOculto}) }
        title="chick para ver el proyecto">
        <span className="cpointer"> {this.state.target.titulo} </span> 

    {/* cambia el estado de archivado */}  
        <span 
          onClick={e => this.setState({ 
            target: {...this.state.target, archivado: !this.state.target.archivado}}, this.updateTarget ) }
          hidden={ !this.state.target.archivado }>
        {nbsp(10)} {iconArchivar}
        </span>        
      </div>

      <div hidden={this.state.tituloOculto}>
        <div  className="form-inline">
          {/** input del título */}
          <input 
            name="titulo"
            value={this.state.target.titulo}
            onChange={ e => this.setState({ 
              target: {...this.state.target, titulo: e.target.value }  
            }) }
            className="form-control"
            placeholder="titulo del objetivo"
          />        
          {/** input de sección */}
          <input 
            name="seccion"
            value={this.state.target.seccion}
            onChange={ e => this.setState({ 
              target: {...this.state.target, seccion: e.target.value }  
            }) }
            className="form-control"
            placeholder="sección"
          />                
          {nbsp(3)}
          <span
            onClick={ e => { this.updateTarget(); this.setState({ tituloOculto: true }) }}> 
            {iconOk}
          </span>        
        </div>
      </div>

      {/* input del html del proyecto */}
      <textarea
        name="html"
        value={this.state.target.html}
        onChange={e => this.setState({ target: { ...this.state.target, html: e.target.value } })}
        hidden={this.state.tituloOculto}
        className="form-control"
      ></textarea>      

    </div>)
  }

  viewDatosTarget = e => {
    if ( this.state.target.archivado === true ) {
      return (<div>
        {this.bloqueTitulo()}
      </div>)
    } else {
      return (
        <div> 
          {this.bloqueTitulo()}
          {this.bloqueFechaInicio()}
          {this.botonesDificultad()}
          <div hidden={this.state.htmlOculto}>
            <br />
            <small>sección {this.state.target.seccion} </small> <br />
            <div dangerouslySetInnerHTML={{ __html: this.state.target.html }} />
          </div>
        </div>    
    )}
}
  
  render(){
    return (
      <div>        
        {this.viewDatosTarget()}
        <hr />
      </div>
    )
  }
} // end class ShowTarget

class ModalTools extends Component {

  render() {
    return (
      <div id="modalTools" className="modal-tools">        
        <div className="modal-tools-content"> 
          <span onClick={e => document.getElementById('modalTools').style = 'display: none'} > {iconEquis} </span>
          <BlogUtils /> 
        </div>
      </div>
    )
  }
}