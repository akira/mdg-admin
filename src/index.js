// React Router y Config
import config from './config.json';
import React  from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
// CSS
import './index.css';
// Components
import { Home } from './App';
import { Test } from './components/Test.js';
// import GoogleApiWrapper  from './components/Test.js';
import { Blog } from './components/Blog';
import Login from './components/Login';
import { Alumnos } from './components/Alumnos';
import { Alumno } from './components/Alumno';
import { Clase } from './components/Clase'
import Targets from './components/Targets';
import ListCursos from './components/ListCursos';
import Notas from './components/Notas.js';
import Game1 from './components/Game1';
import ShowTablasMultiplicar from './components/ShowTablasMultiplicar';
import { BlogUtils } from './components/BlogUtils.js';

// ProMusic
import { GestionSala, TarifasSala, GestionActividad } from './components/GestionSala.js';
import { ListadoSalas } from './components/ListadoSalas';
import { Actuaciones } from './components/Actuaciones';


// Librerías: Stitch, speak-tts, Firebase, etc.
import { Stitch, RemoteMongoClient } from "mongodb-stitch-browser-sdk";
import Speech from 'speak-tts'; // text to speach

// esto lo recomienda firebase para cargar sólo lo que necesita esta app
import firebase from 'firebase/app'
import 'firebase/storage'
import { PromHome } from './components/PromHome.js';
import Vinos from './components/marlene/Vinos.js';

// exportamos stitch para los Components que necesitan stitch.auth.user.id (Blog, Aceites, Alumnos, etc)
export const stitch = Stitch.initializeDefaultAppClient(config.appId);

// y mongo para utilizar comandos de mongo sin pasar por funciones de Stitch como:
// mongo.db('mydb').collection('mycollection').insertOne({})
export const mongo = stitch.getServiceClient(RemoteMongoClient.factory,'mongodb-atlas')



// login con gmail:
// si se ha hecho login con gmail ok, en Application del navegador
// está la clave que ha devuelto Google (redirecResult);
// 
if (stitch.auth.hasRedirectResult()) {
  stitch.auth.handleRedirectResult().then( u => { console.log(u);  window.location.href = '/' })
}

// Firebase functions
firebase.initializeApp(config.firebaseConfig);



export const uploadFile = file => {
  // de momento este Storage no tiene reglas así que funciona sin autenticación;
  // en otro caso habrá que habilitar esto:
  // var provider = new firebase.auth.GoogleAuthProvider();
  // firebase.auth().signInWithPopup(provider);
  let filePath = `/pruebasReact/${file.name}`
  return firebase.storage().ref(filePath).put(file)
  .then( fileSnapshot => {    
    return fileSnapshot.ref.getDownloadURL().then( url => { return url } )   
  })
}


// iconos
export const reactLink = { textDecoration: 'none', color: 'grey'}
export const iconEscuchar = <i className="material-icons" title="escuchar"> hearing </i>


// speak-tts
export const leer = textoALeer => {
  
  let lector = new Speech();  
  lector.init({
      volume: 1,
      lang: "es-ES",
      rate: 0.95, // velocidad de lectura de 0 a 1
      pitch: 0.9   // 1 joven, 0.5 más mayor, 
  }).then( n => { return lector.speak({text: textoALeer}) })  
}


// fetch
export async function exampleAsyncFetch(url, cb) {
  let headers = { 'Accept': 'application/json', 'Content-Type': 'application/json' }
  let method = 'GET'
  let response = await fetch(url, {method: method, headers: headers});
  let data = await response.json()  
  cb(data);
}

export const ajaxGet = (url, cb) => {
  fetch( url, {
    method: 'GET',
    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }
    // body: JSON.stringify(this.state)
  })
  .then( res => { return res.json() })
  .then( data => { cb(data) } )
  // devuelve Promise, dejamos el último then() para que lo haga el Component  
}


// funciones de Stitch
export const findSort = arg => {
  return stitch.callFunction('findAndSort', [{
    db: arg.db, 
    collection: arg.collection, 
    find: arg.find, 
    sort: arg.sort 
  }]);    
}

export const updateOne = arg => {
  return stitch.callFunction('updateOne', [{
    db: arg.db, 
    collection: arg.collection, 
    doc: arg.doc
  }]);    
}

export const insertOne = arg => {
  return stitch.callFunction('insertOne', [{
    db: arg.db, 
    collection: arg.collection, 
    doc: arg.doc
  }]);    
}

export const distinct = arg => {
  // hay un Post que explica que no existe .distinct() en Stitch...
  // por eso lo del $ de abajo
  return stitch.callFunction('distinct', [{
    db: arg.db, 
    collection: arg.collection, 
    field: arg.field // OJO: cuando se llame a esta función el campo de agrupación debe pasarse con un $ delante: p.e. "$titulo"
  }]);    
}


export function nbsp(n) {
  let ret = []
  for (let index = 1; index <= n; index++) {
    ret.push(<span key={index}>&nbsp;</span>);    
  }
  return ret
}


export function br(n) {
  let ret = []
  for (let index = 1; index <= n; index++) {
    ret.push(<br key={index} />);    
  }
  return ret
}

const isLoggedIn = () => {
  if( stitch.auth.isLoggedIn ){ return (
    <div>
      <span> {stitch.auth.user.profile.email} </span>
      (<small> {stitch.auth.user.loggedInProviderName} </small>) 
    </div>)}
  else { return  <span> debes hacer login </span> }
  
}


export const diaSemana = n => {
  let diaSemana = ''
  switch (new Date().getDay()) {
    case 0:
      diaSemana = 'Domingo'
      break;
    case 1:
      diaSemana = 'Lunes'
      break;
    case 2:
      diaSemana = 'Martes'
      break;
    case 3:
      diaSemana = 'Miércoles'
      break;
    case 4:
      diaSemana = 'Jueves'
      break;
    case 5:
      diaSemana = 'Viernes'
      break;
    case 6:
      diaSemana = 'Sábado'
      break;    
    default:
      break;
  }
  return diaSemana;
}


export function jsonToFile(obj = {},  filename = 'archivoSinNombre' ) {
  let e = document.createEvent('MouseEvents'),
      a = document.createElement('a'),        
      blob = null;
  blob = new Blob([JSON.stringify(obj, undefined, 2)], {type: 'text/json'});                 
  a.download = filename;
  a.href = window.URL.createObjectURL(blob);     
  a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');    
  e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
  a.dispatchEvent(e);   
}

export function fileToJson(e, cb){         
  var reader = new FileReader();        
  reader.onload = () => cb(JSON.parse(reader.result));
  reader.readAsText(e.target.files[0]);
}
  


// eslint-disable-next-line
const barra1 = (    
<nav className="navbar navbar-expand barra">
  { /* barra navegación izquierda*/  }
  <ol className="navbar-nav mr-auto">
    <li className="nav-item menu">
      <Link to="/blog" style={reactLink}>Blog</Link>
    </li>
    <li className="nav-item menu">
      <Link to="/alumnos" style={reactLink}>Alumnos</Link>
    </li>
    <li className="nav-item menu">
      <Link to="/login" style={reactLink}>Login</Link>
    </li>
    {nbsp(10)}
    <li style={{color: 'grey'}}>
      {isLoggedIn()}
    </li>        
  </ol>

  { /* barra navegación derecha*/  } 
</nav>)

// eslint-disable-next-line
const barra2 = (
  <div className="menu2">
    <Link to="/blog" style={reactLink}>Blog</Link> {br(1)}
    <Link to="/alumnos" style={reactLink}>Alumnos</Link> {br(1)}
    <Link to="/login" style={reactLink}>Login</Link> {br(1)}
  </div>
)

const routing = (
  <Router>

    {barra1}  

    <Route exact path="/" component={Home} />    
    <Route path="/blog" component={Blog} />
    <Route path="/login" component={Login} />
    <Route path="/home" component={Home} />
    <Route path="/test/" component={Test} />  
    <Route path="/alumnos" component={Alumnos} />  
    <Route path="/alumno/:id" component={Alumno} />  
    <Route path="/clase/:id" component={Clase} />  
    <Route path="/targets/:alumno" component={Targets} />  
    <Route path="/notas/:alumno" component={Notas} />  
    <Route path="/ListCursos" component={ListCursos} />  
    <Route path="/game1" component={Game1} />  
    <Route path="/tabla/:numero" component={ShowTablasMultiplicar} />  
    <Route path="/utils" component={ BlogUtils } />  
    {/* ProMusic */}
    <Route path="/sala/:id" component={ GestionSala } />  
    <Route path="/salas" component={ ListadoSalas } />  
    <Route path="/tarifas/:id" component={ TarifasSala } />  
    <Route path="/actividad/:id" component={ GestionActividad } />  
    <Route path="/calendario" component={ Actuaciones } />  
    <Route path="/promhome" component={ PromHome } />  
    {/* Marlene */}
    <Route path="/vinos" component={ Vinos } />  
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'))

